import config from "./global"

Object.assign(config, {
  appTitle: "Deliasse (dev)",
})

export default config
