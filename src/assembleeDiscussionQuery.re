open Configuration;

open Model;

let component = ReasonReact.statelessComponent("AssembleeDiscussionQuery");

let make =
    (
      ~bibard,
      ~bibardSuffixe,
      ~graphqlClient,
      ~hash,
      ~legislature,
      ~organe,
      ~path,
      _children,
    ) => {
  ...component,
  render: _self =>
    <GraphqlQuery
      graphqlClient
      query=(
        Apollo.createAssembleeDiscussionQuery(
          bibard,
          bibardSuffixe,
          legislature,
          organe,
        )
      )>
      ...(
           response =>
             switch (response) {
             | Loading =>
               <div>
                 <MaterialUI.AppBar position="static">
                   <MaterialUI.Toolbar>
                     <MaterialUI.Typography
                       color=MaterialUI.Typography.Color.Inherit
                       variant=MaterialUI.Typography.Variant.Title>
                       (config.appTitle |> ste)
                     </MaterialUI.Typography>
                   </MaterialUI.Toolbar>
                 </MaterialUI.AppBar>
                 <MaterialUI.LinearProgress />
               </div>
             | Failed(error) =>
               Js.log2("AssembleeDiscussionQuery error", error);
               <div>
                 <MaterialUI.AppBar position="static">
                   <MaterialUI.Toolbar>
                     <MaterialUI.Typography
                       color=MaterialUI.Typography.Color.Inherit
                       variant=MaterialUI.Typography.Variant.Title>
                       (config.appTitle |> ste)
                     </MaterialUI.Typography>
                   </MaterialUI.Toolbar>
                 </MaterialUI.AppBar>
                 <div> ("Error" |> ste) </div>
               </div>;
             | Loaded(data) =>
               let discussion = Decoders.discussionWrapper(data);
               <AssembleeDiscussion discussion graphqlClient hash path />;
             }
         )
    </GraphqlQuery>,
};
