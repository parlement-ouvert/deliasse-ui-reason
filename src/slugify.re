%bs.raw
{|var slugifyier = require("slugify")|};

let slugify: string => string = [%bs.raw
  {|
function (s) {
  return slugifyier(s, {
    replacement: "-",    // replace spaces with replacement
    remove: null,        // regex to remove characters
    lower: true          // result in lower case
  });
}
  |}
];

let slugifySectionNumber = (sectionNumber: string) : string =>
  sectionNumber
  |> slugify
  |> Js.String.split("-")
  |> Array.map(fragment =>
       switch (fragment) {
       | "1er" => "1"
       | "ier" => "i"
       | "premier" => "1"
       | otherFragment => otherFragment
       }
     )
  |> Js.Array.joinWith("-");
