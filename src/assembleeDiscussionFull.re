open Model;

open Types;

type action =
  | CloseLinkedContent
  | OpenLinkedContent(amendement, billPage);

type linkedContentDialog = {
  amendement,
  billPage,
};

type state = {
  linkedContentDialog: option(linkedContentDialog),
  todo: int,
};

let component = ReasonReact.reducerComponent("AssembleeDiscussionFull");

let make = (~discussion: discussion, ~onNavigateToAmendement, _children) => {
  ...component,
  initialState: () => {linkedContentDialog: None, todo: 0},
  reducer: (action, state) =>
    switch (action) {
    | CloseLinkedContent =>
      ReasonReact.Update({...state, linkedContentDialog: None})
    | OpenLinkedContent(amendement, billPage) =>
      ReasonReact.Update({
        ...state,
        linkedContentDialog: Some({amendement, billPage}),
      })
    },
  render: self =>
    <div>
      (
        switch (self.state.linkedContentDialog) {
        | None => ReasonReact.nullElement
        | Some({billPage}) =>
          <MaterialUI.Dialog
            fullScreen=true
            onClose=(_event => self.send(CloseLinkedContent))
            _open=true>
            <MaterialUI.AppBar position="static">
              <MaterialUI.Toolbar
                style=(
                  ReactDOMRe.Style.make(
                    ~justifyContent="space-between",
                    ~display="flex",
                    (),
                  )
                )>
                <MaterialUI.IconButton
                  color=MaterialUI.IconButton.Color.Inherit
                  onClick=(_event => self.send(CloseLinkedContent))>
                  <MaterialUIIcons.Close />
                </MaterialUI.IconButton>
              </MaterialUI.Toolbar>
            </MaterialUI.AppBar>
            <MaterialUI.Typography
              color=MaterialUI.Typography.Color.Inherit
              component="h3"
              variant=MaterialUI.Typography.Variant.Display1>
              (billPage.numero |> ste)
            </MaterialUI.Typography>
            (
              switch (billPage.titre) {
              | None => ReasonReact.nullElement
              | Some(titre) =>
                <MaterialUI.Typography
                  color=MaterialUI.Typography.Color.Inherit
                  component="h4"
                  variant=MaterialUI.Typography.Variant.Title>
                  (titre |> ste)
                </MaterialUI.Typography>
              }
            )
            <div
              dangerouslySetInnerHTML={"__html": billPage.body}
              style=(ReactDOMRe.Style.make(~wordWrap="break-word", ()))
            />
          </MaterialUI.Dialog>
        }
      )
      <AssembleeDiscussionLayout discussion onNavigateToAmendement>
        <div
          style=(
            ReactDOMRe.Style.make(
              ~alignItems="center",
              ~backgroundColor="#f0f0f0",
              ~display="flex",
              ~flexDirection="column",
              (),
            )
          )>
          <MaterialUI.Typography
            color=MaterialUI.Typography.Color.Inherit
            component="h1"
            variant=MaterialUI.Typography.Variant.Display2>
            (discussion.titre |> ste)
          </MaterialUI.Typography>
          <div
            style=(
              ReactDOMRe.Style.make(
                ~alignItems="stretch",
                ~display="flex",
                ~flexDirection="column",
                ~maxWidth="800px",
                (),
              )
            )>
            (
              discussion.divisions
              |> Array.map((division: division) =>
                   <section
                     id=(Slugify.slugify(division.place)) key=division.place>
                     <MaterialUI.Typography
                       align=MaterialUI.Typography.Align.Center
                       color=MaterialUI.Typography.Color.Inherit
                       component="h2"
                       variant=MaterialUI.Typography.Variant.Display1>
                       (division.place |> ste)
                     </MaterialUI.Typography>
                     (
                       discussion.amendements
                       |> Array.to_list
                       |> List.filter((amendement: amendementWrapper) =>
                            amendement.place === division.place
                          )
                       |> Array.of_list
                       |> Array.map((amendementWrapper: amendementWrapper) =>
                            <MaterialUI.Paper
                              elevation=4
                              id=(Slugify.slugify(amendementWrapper.numero))
                              key=amendementWrapper.numero
                              style=(
                                ReactDOMRe.Style.make(
                                  ~marginTop="24px",
                                  ~padding="16px",
                                  (),
                                )
                              )>
                              <AssembleeAmendement
                                amendementWrapper
                                discussion
                                onOpenLinkedContent=(
                                  (amendement, linkedContentPage, _event) =>
                                    self.send(
                                      OpenLinkedContent(
                                        amendement,
                                        linkedContentPage,
                                      ),
                                    )
                                )
                              />
                            </MaterialUI.Paper>
                          )
                       |> ReasonReact.arrayToElement
                     )
                   </section>
                 )
              |> ReasonReact.arrayToElement
            )
          </div>
        </div>
      </AssembleeDiscussionLayout>
    </div>,
};
