open Model;

open Types;

module StringsSetItem = {
  type t = string;
  let compare = Pervasives.compare;
};

module StringsSet = Set.Make(StringsSetItem);

let component = ReasonReact.statelessComponent("AssembleeDiscussionIndex");

let make = (~discussion: discussion, ~onNavigateToAmendement, _children) => {
  ...component,
  render: _self =>
    <AssembleeDiscussionLayout discussion onNavigateToAmendement>
      <MaterialUI.Typography
        color=MaterialUI.Typography.Color.Inherit
        component="h1"
        variant=MaterialUI.Typography.Variant.Display2>
        (discussion.titre |> ste)
      </MaterialUI.Typography>
      (
        discussion.divisions
        |> Array.map((division: division) =>
             <section id=(Slugify.slugify(division.place)) key=division.place>
               <MaterialUI.Typography
                 align=MaterialUI.Typography.Align.Center
                 color=MaterialUI.Typography.Color.Inherit
                 component="h2"
                 variant=MaterialUI.Typography.Variant.Display1>
                 (division.place |> ste)
               </MaterialUI.Typography>
               (
                 discussion.amendements
                 |> Array.to_list
                 |> List.filter((amendement: amendementWrapper) =>
                      amendement.place === division.place
                    )
                 |> Array.of_list
                 |> Array.map((amendementWrapper: amendementWrapper) => {
                      let hash = Slugify.slugify(amendementWrapper.numero);
                      <article key=amendementWrapper.numero>
                        (
                          switch (amendementWrapper.amendement) {
                          | None =>
                            <MaterialUI.Typography
                              color=MaterialUI.Typography.Color.Inherit
                              component="h4"
                              variant=MaterialUI.Typography.Variant.Title>
                              (
                                "Amendement " ++ amendementWrapper.numero |> ste
                              )
                            </MaterialUI.Typography>
                          | Some(amendement) =>
                            <a
                              href=("tout#" ++ hash)
                              onClick=(
                                event => {
                                  ReactEventRe.Mouse.preventDefault(event);
                                  ReactEventRe.Mouse.stopPropagation(event);
                                  onNavigateToAmendement(
                                    amendement.numeroReference,
                                  );
                                }
                              )>
                              <MaterialUI.Typography
                                color=MaterialUI.Typography.Color.Inherit
                                component="h4"
                                variant=MaterialUI.Typography.Variant.Title>
                                ("Amendement " ++ amendement.numeroLong |> ste)
                              </MaterialUI.Typography>
                            </a>
                          }
                        )
                        (
                          switch (discussion.numeroProchainADiscuter) {
                          | None => ReasonReact.nullElement
                          | Some(numeroProchainADiscuter) =>
                            if (numeroProchainADiscuter
                                == amendementWrapper.numero) {
                              <span
                                style=(
                                  ReactDOMRe.Style.make(~color="orange", ())
                                )>
                                <MaterialUIIcons.Whatshot />
                              </span>;
                            } else {
                              ReasonReact.nullElement;
                            }
                          }
                        )
                        <AssembleeSortEnSeance
                          sortEnSeance=amendementWrapper.sort
                        />
                      </article>;
                    })
                 |> ReasonReact.arrayToElement
               )
             </section>
           )
        |> ReasonReact.arrayToElement
      )
    </AssembleeDiscussionLayout>,
};
