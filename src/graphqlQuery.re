open Model;

external castResponse : string => {. "data": Js.Json.t} = "%identity";

type action =
  | Error(Js.Promise.error)
  | Result(string);

type response =
  | Loading
  | Loaded(Js.Json.t)
  | Failed(Js.Promise.error);

type state = {
  response,
  variables: Js.Json.t,
};

let sendQuery = (~graphqlClient, ~query, ~send) => {
  let _ =
    Js.Promise.(
      resolve(graphqlClient##query(query))
      |> then_(value => {
           send(Result(value));
           resolve();
         })
      |> catch(error => {
           send(Error(error));
           resolve();
         })
    );
  ();
};

let component = ReasonReact.reducerComponent("GraphqlQuery");

let make = (~graphqlClient, ~query, children) => {
  ...component,
  initialState: () => {response: Loading, variables: query##variables},
  reducer: (action, state) =>
    switch (action) {
    | Error(error) =>
      ReasonReact.Update({...state, response: Failed(error)})
    | Result(result) =>
      let data = castResponse(result)##data;
      ReasonReact.Update({...state, response: Loaded(data)});
    },
  willReceiveProps: ({send, state}) =>
    if (!
          shallowEqual(
            asJsObject(query##variables),
            asJsObject(state.variables),
          )) {
      sendQuery(~graphqlClient, ~query, ~send);
      state;
    } else {
      state;
    },
  didMount: ({send}) => {
    sendQuery(~graphqlClient, ~query, ~send);
    ReasonReact.NoUpdate;
  },
  render: ({state}) => children(state.response),
};
