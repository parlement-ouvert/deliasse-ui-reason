open Model;

open Types;

[%bs.raw {|require('./amendement.css')|}];

[@bs.module] external logoGouvernement : string = "./logo-gouvernement.png";

let component = ReasonReact.statelessComponent("SenatAmendement");

let make =
    (
      ~amd: ameliAmd,
      ~txt as _txt: ameliTxt,
      /* ~onOpenLinkedContent, */
      _children,
    ) => {
  ...component,
  render: _self =>
    <article
      id=(
        Slugify.slugify(
          Belt.Option.getWithDefault(amd.num, string_of_int(amd.id)),
        )
      )>
      <div
        style=(
          ReactDOMRe.Style.make(
            ~justifyContent="space-between",
            ~display="flex",
            (),
          )
        )>
        <div>
          (
            switch (amd.sub) {
            | None => ReasonReact.nullElement
            | Some(sub) =>
              <MaterialUI.Typography
                color=MaterialUI.Typography.Color.Inherit
                component="h4"
                variant=MaterialUI.Typography.Variant.Subheading>
                (
                  Belt.Option.getWithDefault(sub.lib, string_of_int(sub.id))
                  |> ste
                )
              </MaterialUI.Typography>
            }
          )
          <MaterialUI.Typography
            color=MaterialUI.Typography.Color.Inherit
            component="h3"
            style=(ReactDOMRe.Style.make(~marginBottom="16px", ()))
            variant=MaterialUI.Typography.Variant.Display1>
            (
              "Amendement "
              ++ Belt.Option.getWithDefault(amd.num, string_of_int(amd.id))
              |> ste
            )
            /* (
                 switch discussion.numeroProchainADiscuter {
                 | None => ReasonReact.nullElement
                 | Some(numeroProchainADiscuter) =>
                   if (numeroProchainADiscuter == amd.numero) {
                     <span style=(ReactDOMRe.Style.make(~color="orange", ()))>
                       <MaterialUIIcons.Whatshot />
                     </span>;
                   } else {
                     ReasonReact.nullElement;
                   }
                 }
               ) */
            <SenatSortEnSeance sor=amd.sor />
          </MaterialUI.Typography>
        </div>
      </div>
      /* {
           let placeRe = [%re
             "/(article|chapitre|titre)\\s+(\\d+|[cilvx]+|1er|Ier|premier)(\\s+([a-f]|bis|ter|quater|quinquies))?/i"
           ];
           let placeMatch =
             Js.Re.exec(amd.place, placeRe)
             |> (
               fun
               | None => None
               | Some(result) => Js.Nullable.toOption(Js.Re.captures(result)[0])
             );
           switch placeMatch {
           | None => ReasonReact.nullElement
           | Some(place) =>
             let placeSlug = Slugify.slugifySectionNumber(place);
             switch (Hashtbl.find(discussion.pages, placeSlug)) {
             | linkedContentPage =>
               <MaterialUI.Button
                 onClick=(onOpenLinkedContent(amendement, linkedContentPage))
                 title="Lire l'article du projet de loi."
                 variant=MaterialUI.Button.Variant.Fab>
                 <MaterialUIIcons.LibraryBooks />
               </MaterialUI.Button>
             | exception Not_found => ReasonReact.nullElement
             };
           };
         } */
      <ul>
        <li>
          (
            (
              amd.accgou ?
                {js|Amendement déposé avec l'accord du gouvernement|js} :
                {js|Amendement déposé sans l'accord du gouvernement|js}
            )
            |> ste
          )
        </li>
        (
          /* Motif de retrait ou d'irrecevabilité */
          switch (amd.mot) {
          | None => ReasonReact.nullElement
          | Some(mot) =>
            let motTrimmed = String.trim(mot);
            String.length(motTrimmed) === 0 ?
              ReasonReact.nullElement :
              <li
                dangerouslySetInnerHTML={"__html": motTrimmed}
                style=(ReactDOMRe.Style.make(~wordWrap="break-word", ()))
              />;
          }
        )
        <li>
          (
            {js|Avis de la commission : |js}
            ++ (
              switch (amd.avc) {
              | None => ""
              | Some(avi) => avi.lib
              }
            )
            |> ste
          )
        </li>
        <li>
          (
            {js|Avis du gouvernement : |js}
            ++ (
              switch (amd.avg) {
              | None => ""
              | Some(avi) => avi.lib
              }
            )
            |> ste
          )
        </li>
      </ul>
      (
        switch (amd.obs) {
        | None => ReasonReact.nullElement
        | Some(obs) =>
          let obsTrimmed = String.trim(obs);
          String.length(obsTrimmed) === 0 ?
            ReasonReact.nullElement :
            <div>
              <h4 style=(ReactDOMRe.Style.make(~textTransform="uppercase", ()))>
                ("Observation" |> ste)
              </h4>
              <div
                dangerouslySetInnerHTML={"__html": obsTrimmed}
                style=(ReactDOMRe.Style.make(~wordWrap="break-word", ()))
              />
            </div>;
        }
      )
      (
        switch (amd.dis) {
        | None => ReasonReact.nullElement
        | Some(dis) =>
          <div>
            <h4 style=(ReactDOMRe.Style.make(~textTransform="uppercase", ()))>
              ("Titre" |> ste)
            </h4>
            <div
              dangerouslySetInnerHTML={"__html": dis}
              style=(ReactDOMRe.Style.make(~wordWrap="break-word", ()))
            />
          </div>
        }
      )
      (
        switch (amd.obj) {
        | None => ReasonReact.nullElement
        | Some(obj) =>
          <div
            style=(
              ReactDOMRe.Style.make(
                ~backgroundColor="#ebebeb",
                ~borderBottom="3px solid #979797",
                ~padding="10px",
                (),
              )
            )>
            <h4 style=(ReactDOMRe.Style.make(~textTransform="uppercase", ()))>
              ({js|Exposé sommaire|js} |> ste)
            </h4>
            <div
              dangerouslySetInnerHTML={"__html": obj}
              style=(ReactDOMRe.Style.make(~wordWrap="break-word", ()))
            />
          </div>
        }
      )
      {
        switch (amd.noment) {
        | None => ReasonReact.nullElement
        | Some(AmeliCab(_cab)) =>
          /* Note: The only value for ameliCab is "Gouvernement". */
          <div
            style=(
              ReactDOMRe.Style.make(
                ~alignItems="center",
                ~display="inline-flex",
                ~margin="0px auto",
                ()
              )
            )>
            <img
              alt="Logo du Gouvernement"
              src=logoGouvernement
              style=(
                ReactDOMRe.Style.make(
                  ~flex="1",
                  ~height="100px",
                  ~width="auto",
                  ()
                )
              )
              title="Le Gouvernement"
            />
            <div
              style=(
                ReactDOMRe.Style.make(~flex="10", ~paddingLeft="10px", ())
              )>
              ({js|Amendement proposé par le Gouvernement|js} |> ste)
            </div>
          </div>
        | Some(AmeliCom(com)) =>
          <div
            style=(
              ReactDOMRe.Style.make(
                ~alignItems="center",
                ~display="inline-flex",
                ~margin="0px auto",
                ()
              )
            )>
            /* <img
              alt=(
                "Portrait de "
                ++ sen.qua
                ++ " "
                ++ sen.prenomuse
                ++ " "
                ++ sen.nomuse
              )
              src=auteur.photoUrl
              style=(
                ReactDOMRe.Style.make(
                  ~flex="1",
                  ~height="100px",
                  ~width="auto",
                  ()
                )
              )
              title=(sen.prenomuse ++ " " ++ sen.nomuse)
            /> */
            <div
              style=(
                ReactDOMRe.Style.make(~flex="10", ~paddingLeft="10px", ())
              )>
              <strong>
                (
                  com.lil
                  |> ste
                )
              </strong>
            </div>
          </div>
        | Some(AmeliGrpPol(grpPol)) =>
          <div
            style=(
              ReactDOMRe.Style.make(
                ~alignItems="center",
                ~display="inline-flex",
                ~margin="0px auto",
                ()
              )
            )>
            /* <img
              alt=(
                "Portrait de "
                ++ sen.qua
                ++ " "
                ++ sen.prenomuse
                ++ " "
                ++ sen.nomuse
              )
              src=auteur.photoUrl
              style=(
                ReactDOMRe.Style.make(
                  ~flex="1",
                  ~height="100px",
                  ~width="auto",
                  ()
                )
              )
              title=(sen.prenomuse ++ " " ++ sen.nomuse)
            /> */
            <div
              style=(
                ReactDOMRe.Style.make(~flex="10", ~paddingLeft="10px", ())
              )>
              <strong>
                (
                  grpPol.lilcou
                  ++ (switch (amd.libgrp) {
                  | None => ""
                  | Some(libgrp) => libgrp
                  })
                  |> ste
                )
              </strong>
            </div>
          </div>
        | Some(AmeliGvt(gvt)) =>
          <div
            style=(
              ReactDOMRe.Style.make(
                ~alignItems="center",
                ~display="inline-flex",
                ~margin="0px auto",
                ()
              )
            )>
            /* <img
              alt=(
                "Portrait de "
                ++ sen.qua
                ++ " "
                ++ sen.prenomuse
                ++ " "
                ++ sen.nomuse
              )
              src=auteur.photoUrl
              style=(
                ReactDOMRe.Style.make(
                  ~flex="1",
                  ~height="100px",
                  ~width="auto",
                  ()
                )
              )
              title=(sen.prenomuse ++ " " ++ sen.nomuse)
            /> */
            <div
              style=(
                ReactDOMRe.Style.make(~flex="10", ~paddingLeft="10px", ())
              )>
              <strong>
                (
                  gvt.qua
                  ++ " "
                  ++ gvt.pre
                  ++ " "
                  ++ gvt.nom
                  |> ste
                )
              </strong>
              <br />
              (
                gvt.tit
                |> ste
              )
            </div>
          </div>
        | Some(AmeliSen(sen)) =>
          <div
            style=(
              ReactDOMRe.Style.make(
                ~alignItems="center",
                ~display="inline-flex",
                ~margin="0px auto",
                ()
              )
            )>
            /* <img
              alt=(
                "Portrait de "
                ++ sen.qua
                ++ " "
                ++ sen.prenomuse
                ++ " "
                ++ sen.nomuse
              )
              src=auteur.photoUrl
              style=(
                ReactDOMRe.Style.make(
                  ~flex="1",
                  ~height="100px",
                  ~width="auto",
                  ()
                )
              )
              title=(sen.prenomuse ++ " " ++ sen.nomuse)
            /> */
            <div
              style=(
                ReactDOMRe.Style.make(~flex="10", ~paddingLeft="10px", ())
              )>
              <strong>
                (
                  sen.qua
                  ++ " "
                  ++ sen.prenomuse
                  ++ " "
                  ++ sen.nomuse
                  /* ++ (
                    switch auteur.qualite {
                    | None => ""
                    | Some(qualite) => qualite
                    }
                  ) */
                  ++ (amd.autext ? " et plusieurs de ses collègues" : "")
                  |> ste
                )
              </strong>
              <br />
              (
                sen.grp.lilcou
                |> ste
              )
            </div>
          </div>
        }
      }
  /* (
       switch amendement.cosignataires {
       | [||] => ReasonReact.nullElement
       | cosignataires =>
         <div>
           <h4>
             (
               string_of_int(Array.length(cosignataires))
               ++ " Co-signataires :"
               |> ste
             )
           </h4>
           <ul className="amendement-cosignataires">
             (
               cosignataires
               |> Array.mapi((i, cosignataire: auteur) =>
                    <li key=(string_of_int(i))>
                      (
                        cosignataire.civilite
                        ++ " "
                        ++ cosignataire.prenom
                        ++ " "
                        ++ cosignataire.nom
                        ++ (
                          switch cosignataire.qualite {
                          | None => ""
                          | Some(qualite) => qualite
                          }
                        )
                        ++ (
                          switch amendement.cosignatairesMentionLibre {
                          | None => ""
                          | Some(cosignatairesMentionLibre) =>
                            " " ++ cosignatairesMentionLibre.titre
                          }
                        )
                        |> ste
                      )
                    </li>
                  )
               |> ReasonReact.arrayToElement
             )
           </ul>
         </div>
       }
     ) */
    </article>
};
