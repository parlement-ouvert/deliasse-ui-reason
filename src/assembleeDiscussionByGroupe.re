open Model;

open Types;

module StringsSetItem = {
  type t = string;
  let compare = Pervasives.compare;
};

module StringsSet = Set.Make(StringsSetItem);

let component = ReasonReact.statelessComponent("AssembleeDiscussionByGroupe");

let make = (~discussion: discussion, ~onNavigateToAmendement, _children) => {
  ...component,
  render: _self => {
    let _groupeAuteurCouples =
      discussion.amendements
      |> Array.map((amendementWrapper: amendementWrapper) =>
           (amendementWrapper.auteurGroupe, amendementWrapper.auteurLabel)
         );
    <AssembleeDiscussionLayout discussion onNavigateToAmendement>
      <MaterialUI.Typography
        color=MaterialUI.Typography.Color.Inherit
        component="h1"
        variant=MaterialUI.Typography.Variant.Display2>
        (discussion.titre |> ste)
      </MaterialUI.Typography>
      {
        let groupes = ref(GroupesSet.empty);
        discussion.amendements
        |> Array.iter((amendementWrapper: amendementWrapper) =>
             groupes :=
               GroupesSet.add(amendementWrapper.auteurGroupe, groupes^)
           );
        groupes^
        |> GroupesSet.elements
        |> List.map(groupe =>
             <section key=(shortTitleFromGroupe(groupe))>
               <MaterialUI.Typography
                 color=MaterialUI.Typography.Color.Inherit
                 component="h2"
                 variant=MaterialUI.Typography.Variant.Display1>
                 (shortTitleFromGroupe(groupe) |> ste)
               </MaterialUI.Typography>
               {
                 let auteurs = ref(StringsSet.empty);
                 discussion.amendements
                 |> Array.iter((amendementWrapper: amendementWrapper) =>
                      if (amendementWrapper.auteurGroupe == groupe) {
                        auteurs :=
                          StringsSet.add(
                            amendementWrapper.auteurLabel,
                            auteurs^,
                          );
                      }
                    );
                 auteurs^
                 |> StringsSet.elements
                 |> List.map(auteur =>
                      <section key=auteur>
                        <MaterialUI.Typography
                          color=MaterialUI.Typography.Color.Inherit
                          component="h3"
                          variant=MaterialUI.Typography.Variant.Headline>
                          (auteur |> ste)
                        </MaterialUI.Typography>
                        (
                          discussion.amendements
                          |> Array.to_list
                          |> List.filter(
                               (amendementWrapper: amendementWrapper) =>
                               amendementWrapper.auteurLabel === auteur
                             )
                          |> List.map((amendementWrapper: amendementWrapper) => {
                               let hash =
                                 Slugify.slugify(amendementWrapper.numero);
                               <article key=amendementWrapper.numero>
                                 (
                                   switch (amendementWrapper.amendement) {
                                   | None =>
                                     <MaterialUI.Typography
                                       color=MaterialUI.Typography.Color.Inherit
                                       component="h4"
                                       variant=MaterialUI.Typography.Variant.Title>
                                       (
                                         "Amendement "
                                         ++ amendementWrapper.numero
                                         |> ste
                                       )
                                     </MaterialUI.Typography>
                                   | Some(amendement) =>
                                     <a
                                       href=("tout#" ++ hash)
                                       onClick=(
                                         event => {
                                           ReactEventRe.Mouse.preventDefault(
                                             event,
                                           );
                                           ReactEventRe.Mouse.stopPropagation(
                                             event,
                                           );
                                           onNavigateToAmendement(
                                             amendement.numeroReference,
                                           );
                                         }
                                       )>
                                       <MaterialUI.Typography
                                         color=MaterialUI.Typography.Color.Inherit
                                         component="h4"
                                         variant=MaterialUI.Typography.Variant.Title>
                                         (
                                           "Amendement "
                                           ++ amendement.numeroLong
                                           |> ste
                                         )
                                       </MaterialUI.Typography>
                                     </a>
                                   }
                                 )
                                 (
                                   switch (discussion.numeroProchainADiscuter) {
                                   | None => ReasonReact.nullElement
                                   | Some(numeroProchainADiscuter) =>
                                     if (numeroProchainADiscuter
                                         == amendementWrapper.numero) {
                                       <span
                                         style=(
                                           ReactDOMRe.Style.make(
                                             ~color="orange",
                                             (),
                                           )
                                         )>
                                         <MaterialUIIcons.Whatshot />
                                       </span>;
                                     } else {
                                       ReasonReact.nullElement;
                                     }
                                   }
                                 )
                                 <AssembleeSortEnSeance
                                   sortEnSeance=amendementWrapper.sort
                                 />
                               </article>;
                             })
                          |> Array.of_list
                          |> ReasonReact.arrayToElement
                        )
                      </section>
                    )
                 |> Array.of_list
                 |> ReasonReact.arrayToElement;
               }
             </section>
           )
        |> Array.of_list
        |> ReasonReact.arrayToElement;
      }
    </AssembleeDiscussionLayout>;
  },
};
