open Model;

open Types;

[%bs.raw {|require('./amendement.css')|}];

[@bs.module] external logoGouvernement : string = "./logo-gouvernement.png";

let component = ReasonReact.statelessComponent("AssembleeAmendement");

let make =
    (
      ~amendementWrapper: amendementWrapper,
      ~discussion: discussion,
      ~onOpenLinkedContent,
      _children,
    ) => {
  ...component,
  render: _self =>
    switch (amendementWrapper.amendement) {
    | None =>
      <article id=(Slugify.slugify(amendementWrapper.numero))>
        <MaterialUI.Typography
          color=MaterialUI.Typography.Color.Inherit
          component="h4"
          variant=MaterialUI.Typography.Variant.Subheading>
          (amendementWrapper.place |> ste)
        </MaterialUI.Typography>
        <MaterialUI.Typography
          color=MaterialUI.Typography.Color.Inherit
          component="h3"
          style=(ReactDOMRe.Style.make(~marginBottom="16px", ()))
          variant=MaterialUI.Typography.Variant.Display1>
          ("Amendement " ++ amendementWrapper.numero |> ste)
          (
            switch (discussion.numeroProchainADiscuter) {
            | None => ReasonReact.nullElement
            | Some(numeroProchainADiscuter) =>
              if (numeroProchainADiscuter == amendementWrapper.numero) {
                <span style=(ReactDOMRe.Style.make(~color="orange", ()))>
                  <MaterialUIIcons.Whatshot />
                </span>;
              } else {
                ReasonReact.nullElement;
              }
            }
          )
          <AssembleeSortEnSeance sortEnSeance=amendementWrapper.sort />
        </MaterialUI.Typography>
        <MaterialUI.LinearProgress />
      </article>
    | Some(amendement) =>
      <article id=(Slugify.slugify(amendementWrapper.numero))>
        <div
          style=(
            ReactDOMRe.Style.make(
              ~justifyContent="space-between",
              ~display="flex",
              (),
            )
          )>
          <div>
            <MaterialUI.Typography
              color=MaterialUI.Typography.Color.Inherit
              component="h4"
              variant=MaterialUI.Typography.Variant.Subheading>
              (amendementWrapper.place |> ste)
            </MaterialUI.Typography>
            <MaterialUI.Typography
              color=MaterialUI.Typography.Color.Inherit
              component="h3"
              style=(ReactDOMRe.Style.make(~marginBottom="16px", ()))
              variant=MaterialUI.Typography.Variant.Display1>
              ("Amendement " ++ amendement.numeroLong |> ste)
              (
                switch (discussion.numeroProchainADiscuter) {
                | None => ReasonReact.nullElement
                | Some(numeroProchainADiscuter) =>
                  if (numeroProchainADiscuter == amendementWrapper.numero) {
                    <span style=(ReactDOMRe.Style.make(~color="orange", ()))>
                      <MaterialUIIcons.Whatshot />
                    </span>;
                  } else {
                    ReasonReact.nullElement;
                  }
                }
              )
              <AssembleeSortEnSeance sortEnSeance=amendement.sortEnSeance />
            </MaterialUI.Typography>
          </div>
          {
            let placeRe = [%re
              "/(article|chapitre|titre)\\s+(\\d+|[cilvx]+|1er|Ier|premier)(\\s+([a-f]|bis|ter|quater|quinquies))?/i"
            ];
            let placeMatch =
              Js.Re.exec(amendementWrapper.place, placeRe)
              |> (
                fun
                | None => None
                | Some(result) =>
                  Js.Nullable.toOption(Js.Re.captures(result)[0])
              );
            switch (placeMatch) {
            | None => ReasonReact.nullElement
            | Some(place) =>
              let placeSlug = Slugify.slugifySectionNumber(place);
              switch (Hashtbl.find(discussion.pages, placeSlug)) {
              | linkedContentPage =>
                <MaterialUI.Button
                  onClick=(onOpenLinkedContent(amendement, linkedContentPage))
                  title="Lire l'article du projet de loi."
                  variant=MaterialUI.Button.Variant.Fab>
                  <MaterialUIIcons.LibraryBooks />
                </MaterialUI.Button>
              | exception Not_found => ReasonReact.nullElement
              };
            };
          }
        </div>
        (
          switch (amendement.dispositif) {
          | None => ReasonReact.nullElement
          | Some(dispositif) =>
            <div>
              <h4
                style=(ReactDOMRe.Style.make(~textTransform="uppercase", ()))>
                ("Titre" |> ste)
              </h4>
              <div
                dangerouslySetInnerHTML={"__html": dispositif}
                style=(ReactDOMRe.Style.make(~wordWrap="break-word", ()))
              />
            </div>
          }
        )
        (
          switch (amendement.exposeSommaire) {
          | None => ReasonReact.nullElement
          | Some(exposeSommaire) =>
            <div
              style=(
                ReactDOMRe.Style.make(
                  ~backgroundColor="#ebebeb",
                  ~borderBottom="3px solid #979797",
                  ~padding="10px",
                  (),
                )
              )>
              <h4
                style=(ReactDOMRe.Style.make(~textTransform="uppercase", ()))>
                ({js|Exposé sommaire|js} |> ste)
              </h4>
              <div
                dangerouslySetInnerHTML={"__html": exposeSommaire}
                style=(ReactDOMRe.Style.make(~wordWrap="break-word", ()))
              />
            </div>
          }
        )
        {
          let auteur = amendement.auteur;
          if (auteur.estGouvernement) {
            <div
              style=(
                ReactDOMRe.Style.make(
                  ~alignItems="center",
                  ~display="inline-flex",
                  ~margin="0px auto",
                  (),
                )
              )>
              <img
                alt="Logo du Gouvernement"
                src=logoGouvernement
                style=(
                  ReactDOMRe.Style.make(
                    ~flex="1",
                    ~height="100px",
                    ~width="auto",
                    (),
                  )
                )
                title="Le Gouvernement"
              />
              <div
                style=(
                  ReactDOMRe.Style.make(~flex="10", ~paddingLeft="10px", ())
                )>
                ({js|Amendement proposé par le Gouvernement|js} |> ste)
              </div>
            </div>;
          } else {
            <div
              style=(
                ReactDOMRe.Style.make(
                  ~alignItems="center",
                  ~display="inline-flex",
                  ~margin="0px auto",
                  (),
                )
              )>
              <img
                alt=(
                  "Portrait de "
                  ++ auteur.civilite
                  ++ " "
                  ++ auteur.prenom
                  ++ " "
                  ++ auteur.nom
                )
                src=auteur.photoUrl
                style=(
                  ReactDOMRe.Style.make(
                    ~flex="1",
                    ~height="100px",
                    ~width="auto",
                    (),
                  )
                )
                title=(auteur.prenom ++ " " ++ auteur.nom)
              />
              <div
                style=(
                  ReactDOMRe.Style.make(~flex="10", ~paddingLeft="10px", ())
                )>
                <strong>
                  (
                    auteur.civilite
                    ++ " "
                    ++ auteur.prenom
                    ++ " "
                    ++ auteur.nom
                    ++ (
                      switch (auteur.qualite) {
                      | None => ""
                      | Some(qualite) => qualite
                      }
                    )
                    ++ (
                      switch (amendement.cosignatairesMentionLibre) {
                      | None => ""
                      | Some(cosignatairesMentionLibre) =>
                        " " ++ cosignatairesMentionLibre.titre
                      }
                    )
                    |> ste
                  )
                </strong>
                <br />
                (
                  groupeTitleFromAuteurGroupe(
                    auteur,
                    amendementWrapper.auteurGroupe,
                  )
                  |> ste
                )
              </div>
            </div>;
          };
        }
        (
          switch (amendement.cosignataires) {
          | [||] => ReasonReact.nullElement
          | cosignataires =>
            <div>
              <h4>
                (
                  string_of_int(Array.length(cosignataires))
                  ++ " Co-signataires :"
                  |> ste
                )
              </h4>
              <ul className="amendement-cosignataires">
                (
                  cosignataires
                  |> Array.mapi((i, cosignataire: auteur) =>
                       <li key=(string_of_int(i))>
                         (
                           cosignataire.civilite
                           ++ " "
                           ++ cosignataire.prenom
                           ++ " "
                           ++ cosignataire.nom
                           ++ (
                             switch (cosignataire.qualite) {
                             | None => ""
                             | Some(qualite) => qualite
                             }
                           )
                           ++ (
                             switch (amendement.cosignatairesMentionLibre) {
                             | None => ""
                             | Some(cosignatairesMentionLibre) =>
                               " " ++ cosignatairesMentionLibre.titre
                             }
                           )
                           |> ste
                         )
                       </li>
                     )
                  |> ReasonReact.arrayToElement
                )
              </ul>
            </div>
          }
        )
      </article>
    },
};
