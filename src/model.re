open Types;

external asJsObject : 'a => Js.t({..}) = "%identity";

[@bs.module]
external shallowEqual : (Js.t({..}), Js.t({..})) => bool = "shallowequal";

module Groupe = {
  type t = option(groupe);
  let compare = (groupe1: t, groupe2: t) =>
    switch (groupe1, groupe2) {
    | (None, None) => 0
    | (None, Some(_groupe2)) => 1
    | (Some(_groupe1), None) => (-1)
    | (Some(groupe1), Some(groupe2)) =>
      Pervasives.compare(groupe1, groupe2)
    };
};

module GroupesSet = Set.Make(Groupe);

let assembleeDiscussionUrlPath =
    (
      ~bibard: string,
      ~bibardSuffixe: string,
      ~legislature: int,
      ~organe: string,
    )
    : string =>
  "/assemblee/discussions/"
  ++ string_of_int(legislature)
  ++ "/"
  ++ organe
  ++ "/"
  ++ bibard
  ++ "/"
  ++ bibardSuffixe;

let groupeTitleFromAuteurGroupe = (auteur: auteur, groupe: option(groupe)) =>
  switch (groupe) {
  | None => "" /* Gouvernement... */
  | Some(GaucheDemocrateEtRepublicaine) => {js|Groupe de la Gauche démocrate et républicaine|js}
  | Some(LaFranceInsoumise) => {js|Groupe La France insoumise|js}
  | Some(LaRepubliqueEnMarche) => {js|Groupe La République en Marche|js}
  | Some(LesRepublicains) => {js|Groupe Les Républicains|js}
  | Some(MouvementDemocrateEtApparentes) => {js|Groupe du Mouvement Démocrate et apparentés|js}
  | Some(NonInscrits) =>
    if (auteur.civilite === "Mme") {
      {js|Non inscrite|js};
    } else {
      {js|Non inscrit|js};
    }
  | Some(NouvelleGauche) => {js|Groupe Nouvelle Gauche|js}
  | Some(UdiAgirEtIndependants) => {js|Groupe UDI, Agir et Indépendants|js}
  };

let senatDiscussionUrlPath =
    (~num: string, ~sesinsLil: string, ~txttyp: string)
    : string =>
  "/senat/discussions/" ++ txttyp ++ "/" ++ sesinsLil ++ "/" ++ num;

let shortTitleFromGroupe = (groupe: option(groupe)) =>
  switch (groupe) {
  | None => "~~~" /* Gouvernement... */
  | Some(GaucheDemocrateEtRepublicaine) => {js|Gauche démocrate et républicaine|js}
  | Some(LaFranceInsoumise) => {js|La France insoumise|js}
  | Some(LaRepubliqueEnMarche) => {js|La République en Marche|js}
  | Some(LesRepublicains) => {js|Les Républicains|js}
  | Some(MouvementDemocrateEtApparentes) => {js|Mouvement Démocrate et apparentés|js}
  | Some(NonInscrits) => {js|Non inscrits|js}
  | Some(NouvelleGauche) => {js|Nouvelle Gauche|js}
  | Some(UdiAgirEtIndependants) => {js|UDI, Agir et Indépendants|js}
  };

let ste = ReasonReact.stringToElement;

let urlPathFromAssembleeDiscussion = (discussion: discussion) : string =>
  assembleeDiscussionUrlPath(
    ~bibard=discussion.bibard,
    ~bibardSuffixe=discussion.bibardSuffixe,
    ~legislature=discussion.legislature,
    ~organe=discussion.organe,
  );

let urlPathFromSenatDiscussion = (txt: ameliTxt) : string =>
  switch (txt.sesins) {
  | None => "TODO"
  | Some(sesins) =>
    senatDiscussionUrlPath(
      ~num=txt.num,
      ~sesinsLil=sesins.lil,
      ~txttyp=txt.txttyp,
    )
  };
