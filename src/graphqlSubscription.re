external castResponse : string => {. "data": Js.Json.t} = "%identity";

let subscribeJs:
  (Apollo.graphqlClient, ApolloClient.queryObj, string => unit) => Js.t('a) = [%bs.raw
  {|
function subscribe(graphqlClient, query, handler) {
  return graphqlClient.subscribe(query).subscribe({
    next: function (result) {
      handler(result);
    }
  });
}
  |}
];

let subscribe =
    (
      ~graphqlClient: Apollo.graphqlClient,
      ~query: ApolloClient.queryObj,
      ~receiver,
    )
    : Js.t('a) =>
  subscribeJs(
    graphqlClient,
    query,
    (result: string) => {
      /* Js.log2("GraphqlSubscription result:", result); */
      let data = castResponse(result)##data;
      receiver(data);
    },
  );

let unsubscribe = (subscription: Js.t('a)) : unit =>
  subscription##unsubscribe();
