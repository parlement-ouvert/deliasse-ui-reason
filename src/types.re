type etat =
  | Accepte
  | Discute;

type groupe =
  | GaucheDemocrateEtRepublicaine
  | LaFranceInsoumise
  | LaRepubliqueEnMarche
  | LesRepublicains
  | MouvementDemocrateEtApparentes
  | NouvelleGauche
  | UdiAgirEtIndependants
  | NonInscrits;

type sortEnSeance =
  | Adopte
  | Depose
  | NonSoutenu
  | Rejete
  | Retire
  | Tombe;

type typeDiscussion =
  | ProjetDeLoi;

type ameliAvi = {
  id: string,
  lib: string,
  cod: string,
};

type ameliCab = {
  id: int,
  typ: string,
  act: string,
  entid: int,
  codint: string,
  lilOptional: option(string),
};

type ameliCom = {
  id: int,
  typ: string,
  act: string,
  entid: int,
  cod: string,
  lib: string,
  lil: string,
  spc: string,
  codint: string,
  tri: option(int),
};

type ameliGrpPol = {
  id: int,
  typ: string,
  act: string,
  entid: int,
  cod: string,
  libcou: string,
  lilcou: string,
  codint: string,
  tri: option(int),
};

type ameliGvt = {
  id: int,
  typ: string,
  act: string,
  entid: int,
  nom: string,
  pre: string,
  qua: string,
  tit: string,
};

type ameliSen = {
  id: int,
  typ: string,
  act: string,
  entid: int,
  grp: ameliGrpPol,
  grpid: int,
  comid: option(int),
  comspcid: option(int),
  mat: string,
  qua: string,
  nomuse: string,
  prenomuse: string,
  nomtec: option(string),
  hom: option(string),
  app: option(string),
  ratt: option(string),
  nomusemin: string,
  senfem: option(string),
};

type ameliEnt =
  | AmeliCab(ameliCab)
  | AmeliCom(ameliCom)
  | AmeliGrpPol(ameliGrpPol)
  | AmeliGvt(ameliGvt)
  | AmeliSen(ameliSen);

type ameliSor = {
  id: string,
  lib: string,
  cod: string,
  typ: string,
};

type ameliTypSub = {
  id: int,
  lib: string,
};

type ameliSub = {
  id: int,
  txtid: int,
  merid: option(int),
  typ: option(ameliTypSub),
  typid: option(int),
  lic: option(string),
  lib: option(string),
  pos: option(int),
  sig_: option(string),
  posder: option(int),
  prires: option(int),
  dupl: string,
  subamd: string,
  sorid: option(string),
  txtidder: option(int),
  style: string,
};

type ameliAmd = {
  id: int,
  sub: option(ameliSub),
  subid: option(int),
  amdperid: option(int),
  motid: option(int),
  etaid: int,
  noment: option(ameliEnt),
  nomentid: int,
  sor: option(ameliSor),
  sorid: option(string),
  avc: option(ameliAvi),
  avcid: option(string),
  avg: option(ameliAvi),
  avgid: option(string),
  irrid: option(int),
  /* txt: ameliTxt, */
  txtid: int,
  opmid: option(int),
  ocmid: option(int),
  ideid: option(int),
  discomid: option(int),
  num: option(string),
  rev: int,
  typ: string,
  dis: option(string),
  obj: option(string),
  datdep: option(string),
  obs: option(string),
  ord: option(int),
  autext: bool,
  subpos: option(int),
  mot: option(string),
  numabs: option(int),
  subidder: option(int),
  libgrp: option(string),
  alinea: option(int),
  accgou: bool,
  colleg: bool,
  typrectid: option(int),
  islu: bool,
  motposexa: string,
};

type ameliNat = {
  id: int,
  lib: string,
};

type ameliSes = {
  id: int,
  typid: int,
  ann: int,
  lil: string,
};

type ameliTxt = {
  id: int,
  nat: ameliNat,
  natid: int,
  lecid: int,
  sesins: option(ameliSes),
  sesinsid: option(int),
  sesdep: ameliSes,
  sesdepid: int,
  fbuid: option(int),
  num: string,
  int: string,
  inl: option(string),
  datdep: string,
  urg: string,
  dis: string,
  secdel: string,
  loifin: string,
  loifinpar: option(int),
  txtamd: string,
  datado: option(string),
  numado: option(int),
  txtexa: option(string),
  pubdellim: option(string),
  numabs: option(int),
  libdelim: option(string),
  libcplnat: option(string),
  doslegsignet: option(string),
  proacc: string,
  txttyp: string,
  ordsnddelib: option(string),
  txtetaid: int,
  fusderid: option(int),
  fusder: string,
  fusderord: int,
  fusdertyp: option(string),
  amds: array(ameliAmd),
  subs: array(ameliSub),
};

type auteur = {
  civilite: string,
  estRapporteur: bool,
  estGouvernement: bool,
  groupeTribunId: string,
  nom: string,
  photoUrl: string,
  prenom: string,
  qualite: option(string),
  tribunId: string,
};

type cosignatairesMentionLibre = {titre: string};

type amendement = {
  auteur,
  bibard: string,
  bibardSuffixe: string,
  cosignataires: array(auteur),
  cosignatairesMentionLibre: option(cosignatairesMentionLibre),
  dispositif: option(string),
  etat,
  exposeSommaire: option(string),
  legislature: int,
  numero: string,
  numeroLong: string,
  numeroReference: string,
  organeAbrv: string,
  place: string,
  placeReference: string,
  sortEnSeance,
};

type amendementWrapper = {
  alineaLabel: string,
  amendement: option(amendement),
  auteurGroupe: option(groupe),
  auteurLabel: string,
  discussionCommune: string,
  discussionCommuneAmdtPositon: string,
  discussionCommuneSsAmdtPositon: string,
  discussionIdentique: string,
  discussionIdentiqueAmdtPositon: string,
  discussionIdentiqueSsAmdtPositon: string,
  missionLabel: string,
  numero: string,
  parentNumero: string,
  place: string,
  position: string,
  sort: sortEnSeance,
};

type billPage = {
  body: string,
  numero: string,
  slug: string,
  titre: option(string),
};

type config = {
  apiUrl: string,
  appTitle: string,
  appUrl: string,
  twitterName: string,
};

type division = {
  /* amendements */
  place: string,
  position: string,
};

type discussion = {
  amendements: array(amendementWrapper),
  bibard: string,
  bibardSuffixe: string,
  divisions: array(division),
  legislature: int,
  numeroProchainADiscuter: option(string),
  organe: string,
  pages: Hashtbl.t(string, billPage),
  titre: string,
  typeDiscussion,
};

type ordreDuJourItem = {
  textBibard: string,
  textBibardSuffixe: string,
  textTitre: string,
};

type prochainADiscuter = {
  bibard: string,
  bibardSuffixe: string,
  legislature: int,
  nbrAmdtRestant: int,
  numAmdt: string,
  organeAbrv: string,
};

type referenceOrgane = {
  text: string,
  value: string,
};
