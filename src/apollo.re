%bs.raw
{|
var ApolloCacheInmemory = require("apollo-cache-inmemory");
var InMemoryCache = ApolloCacheInmemory.InMemoryCache;
var IntrospectionFragmentMatcher = ApolloCacheInmemory.IntrospectionFragmentMatcher;
var ApolloClient = require("apollo-client").ApolloClient;
var ApolloLink = require("apollo-link").ApolloLink;
var HttpLink = require("apollo-link-http").HttpLink;
var WebSocketLink = require("apollo-link-ws").WebSocketLink;
var getOperationAST = require("graphql").getOperationAST;
/* var gql = require("graphql-tag"); */
|};

[@bs.module] external gql : ReasonApolloTypes.gql = "graphql-tag";

%bs.raw
{|
// Cf https://www.apollographql.com/docs/react/recipes/fragment-matching.html
var introspectionQueryResultData = {
  "__schema": {
    "types": [
      {
        "kind": "INTERFACE",
        "name": "AmeliEnt",
        "possibleTypes": [
          {
            "name": "AmeliCab"
          },
          {
            "name": "AmeliCom"
          },
          {
            "name": "AmeliGrpPol"
          },
          {
            "name": "AmeliGvt"
          },
          {
            "name": "AmeliSen"
          }
        ]
      }
    ]
  }
};
|};

let ameliAmdFragment = {js|
    fragment AmeliAmdFragment on AmeliAmd {
      id
      sub {
        ...AmeliSubFragment
      }
      subid
      amdperid
      motid
      etaid
      noment {
        ...AmeliEntFragment
      }
      nomentid
      sor {
        ...AmeliSorFragment
      }
      sorid
      avc {
        ...AmeliAviFragment
      }
      avcid
      avg {
        ...AmeliAviFragment
      }
      avgid
      irrid
      txtid
      opmid
      ocmid
      ideid
      discomid
      num
      rev
      typ
      dis
      obj
      datdep
      obs
      ord
      autext
      subpos
      mot
      numabs
      subidder
      libgrp
      alinea
      accgou
      colleg
      typrectid
      islu
      motposexa
    }
  |js};

let ameliAviFragment = {js|
    fragment AmeliAviFragment on AmeliAvi {
      id
      lib
      cod
    }
  |js};

let ameliGrpPolFragment = {js|
    fragment AmeliGrpPolFragment on AmeliGrpPol {
      id
      typ
      act
      entid
      cod
      libcou
      lilcou
      codint
      tri
    }
  |js};

let ameliEntFragment = {js|
    fragment AmeliEntFragment on AmeliEnt {
      ... on AmeliCab {
        id
        typ
        act
        entid
        codint
        lilOptional: lil
      }
      ... on AmeliCom {
        id
        typ
        act
        entid
        cod
        lib
        lil
        spc
        codint
        tri
      }
      ... on AmeliGrpPol {
        ...AmeliGrpPolFragment
      }
      ... on AmeliGvt {
        id
        typ
        act
        entid
        nom
        pre
        qua
        tit
      }
      ... on AmeliSen {
        id
        typ
        act
        entid
        grp {
          ...AmeliGrpPolFragment
        }
        grpid
        comid
        comspcid
        mat
        qua
        nomuse
        prenomuse
        nomtec
        hom
        app
        ratt
        nomusemin
        senfem
      }
    }
  |js};

let ameliNatFragment = {js|
    fragment AmeliNatFragment on AmeliNat {
      id
      lib
    }
  |js};

let ameliSesFragment = {js|
    fragment AmeliSesFragment on AmeliSes {
      id
      typid
      ann
      lil
    }
  |js};

let ameliSubFragment = {js|
    fragment AmeliSubFragment on AmeliSub {
      id
      txtid
      merid
      typ {
        ...AmeliTypSubFragment
      }
      typid
      lic
      lib
      pos
      sig
      posder
      prires
      dupl
      subamd
      sorid
      txtidder
      style
    }
  |js};

let ameliSorFragment = {js|
    fragment AmeliSorFragment on AmeliSor {
      id
      lib
      cod
      typ
    }
  |js};

let ameliTypSubFragment = {js|
    fragment AmeliTypSubFragment on AmeliTypSub {
      id
      lib
    }
  |js};

let ameliTxtFragment = {js|
    fragment AmeliTxtFragment on AmeliTxt {
      id
      nat {
        ...AmeliNatFragment
      }
      natid
      lecid
      sesins {
        ...AmeliSesFragment
      }
      sesinsid
      sesdep {
        ...AmeliSesFragment
      }
      sesdepid
      fbuid
      num
      int
      inl
      datdep
      urg
      dis
      secdel
      loifin
      loifinpar
      txtamd
      datado
      numado
      txtexa
      pubdellim
      numabs
      libdelim
      libcplnat
      doslegsignet
      proacc
      txttyp
      ordsnddelib
      txtetaid
      fusderid
      fusder
      fusderord
      fusdertyp
    }
  |js};

let amendementFragment = {js|
    fragment AmendementFragment on Amendement {
      auteur {
        ...AuteurFragment
      }
      bibard
      bibardSuffixe
      cosignataires {
        ...AuteurFragment
      }
      cosignatairesMentionLibre {
        ...CosignatairesMentionLibreFragment
      }
      dispositif
      etat
      exposeSommaire
      legislature
      numero
      numeroLong
      numeroReference
      organeAbrv
      place
      placeReference
      sortEnSeance
    }
  |js};

let auteurFragment = {js|
    fragment AuteurFragment on Auteur {
      civilite
      estRapporteur
      estGouvernement
      groupeTribunId
      nom
      photoUrl
      prenom
      qualite
      tribunId
    }
  |js};

let cosignatairesMentionLibreFragment = {js|
    fragment CosignatairesMentionLibreFragment on CosignatairesMentionLibre {
      titre
    }
  |js};

let discussionFragment = {js|
    fragment DiscussionFragment on Discussion {
      amendements {
        ...EnveloppeAmendementFragment
      }
      bibard
      bibardSuffixe
      divisions {
        ...DivisionFragment
      }
      legislature
      numeroProchainADiscuter
      organe
      pages {
        ...PageFragment
      }
      titre
      type
    }
  |js};

let divisionFragment = {js|
    fragment DivisionFragment on Division {
      # amendements
      place
      position
    }
  |js};

let elementOrdreDuJourFragment = {js|
    fragment ElementOrdreDuJourFragment on ElementOrdreDuJour {
      textBibard
      textBibardSuffixe
      textTitre
    }
  |js};

let enveloppeAmendementFragment = {js|
    fragment EnveloppeAmendementFragment on EnveloppeAmendement {
      alineaLabel
      amendement {
        ...AmendementFragment
      }
      auteurGroupe
      auteurLabel
      discussionCommune
      discussionCommuneAmdtPositon
      discussionCommuneSsAmdtPositon
      discussionIdentique
      discussionIdentiqueAmdtPositon
      discussionIdentiqueSsAmdtPositon
      missionLabel
      numero
      parentNumero
      place
      position
      sort
    }
  |js};

let pageFragment = {js|
    fragment PageFragment on Page {
      body
      numero
      slug
      titre
    }
  |js};

let prochainADiscuterFragment = {js|
    fragment ProchainADiscuterFragment on ProchainADiscuter {
      bibard
      bibardSuffixe
      legislature
      nbrAmdtRestant
      numAmdt
      organeAbrv
    }
  |js};

let referenceOrganeFragment = {js|
    fragment ReferenceOrganeFragment on ReferenceOrgane {
      text
      value
    }
  |js};

type graphqlClient = ApolloClient.generatedApolloClient;

let amendementUpsertedSubscriptionQuery =
  gql(.
    {j|
      subscription onAmendementUpserted {
        amendementUpserted {
          ...AmendementFragment
        }
      }
      $(amendementFragment)
      $(auteurFragment)
      $(cosignatairesMentionLibreFragment)
    |j},
  );

let createAmendementUpsertedSubscriptionQuery: unit => ApolloClient.queryObj = [%bs.raw
  {|
function () {
  return {
    query: amendementUpsertedSubscriptionQuery,
    variables: {
    }
  };
}
  |}
];

let assembleeDiscussionQuery =
  gql(.
    {js|
      query Discussion($bibard: String!, $bibardSuffixe: String, $legislature: Int!, $organe: String!) {
        discussion(bibard: $bibard, bibardSuffixe: $bibardSuffixe, legislature: $legislature, organe: $organe) {
          ...DiscussionFragment
        }
      }
    |js}
    ++ amendementFragment
    ++ auteurFragment
    ++ cosignatairesMentionLibreFragment
    ++ discussionFragment
    ++ divisionFragment
    ++ enveloppeAmendementFragment
    ++ pageFragment,
  );

let createAssembleeDiscussionQuery:
  (string, string, string, string) => ApolloClient.queryObj = [%bs.raw
  {|
function (bibard, bibardSuffixe, legislature, organe) {
  legislature = parseInt(legislature);
  return {
    query: assembleeDiscussionQuery,
    variables: {
      bibard: bibard,
      bibardSuffixe: bibardSuffixe,
      legislature: legislature,
      organe: organe
    }
  };
}
  |}
];

let ordreDuJourQuery =
  gql(.
    {js|
      query OrdreDuJour($legislature: Int!, $organe: String!) {
        ordreDuJour(legislature: $legislature, organe: $organe) {
          ...ElementOrdreDuJourFragment
        }
      }
    |js}
    ++ elementOrdreDuJourFragment,
  );

let createOrdreDuJourQuery: (string, string) => ApolloClient.queryObj = [%bs.raw
  {|
function (legislature, organe) {
  legislature = parseInt(legislature);
  return {
    query: ordreDuJourQuery,
    variables: {
      legislature: legislature,
      organe: organe
    }
  };
}
  |}
];

let referencesOrganesQuery =
  gql(.
    {js|
      query ReferencesOrganes($legislature: Int!) {
        referencesOrganes(legislature: $legislature) {
          ...ReferenceOrganeFragment
        }
      }
    |js}
    ++ referenceOrganeFragment,
  );

let createReferencesOrganesQuery: string => ApolloClient.queryObj = [%bs.raw
  {|
function (legislature) {
  legislature = parseInt(legislature);
  return {
    query: referencesOrganesQuery,
    variables: {
      legislature: legislature
    }
  };
}
  |}
];

let prochainADiscuterUpsertedSubscriptionQuery =
  gql(.
    {js|
      subscription onProchainADiscuterUpserted {
        prochainADiscuterUpserted {
          ...ProchainADiscuterFragment
        }
      }
    |js}
    ++ prochainADiscuterFragment,
  );

let createProchainADiscuterUpsertedSubscriptionQuery:
  unit => ApolloClient.queryObj = [%bs.raw
  {|
function () {
  return {
    query: prochainADiscuterUpsertedSubscriptionQuery,
    variables: {
    }
  };
}
  |}
];

let createGraphqlClient: (string, string) => graphqlClient = [%bs.raw
  {|
function (httpUrl, wsUrl) {
  var link = ApolloLink.split(
    function (operation) {
      var operationAST = getOperationAST(operation.query, operation.operationName);
      return !!operationAST && operationAST.operation === "subscription";
    },
    new WebSocketLink({
      uri: wsUrl,
      options: {
        reconnect: true, //auto-reconnect
        // // carry login state (should use secure websockets (wss) when using this)
        // connectionParams: {
        //   authToken: localStorage.getItem("Meteor.loginToken")
        // }
      },
    }),
    new HttpLink({ uri: httpUrl })
  );
  // Finally, create your ApolloClient instance with the modified network interface
  var fragmentMatcher = new IntrospectionFragmentMatcher({
    introspectionQueryResultData
  });
  return new ApolloClient({
    cache: new InMemoryCache({ fragmentMatcher }),
    link: link
  });
}
  |}
];

let senatDiscussionQuery =
  gql(.
    {js|
      query SenatDiscussion($num: String!, $sesinsLil: String!, $txttyp: String!) {
        senatDiscussion(num: $num, sesinsLil: $sesinsLil, txttyp: $txttyp) {
          ...AmeliTxtFragment
          amds {
            ...AmeliAmdFragment
          }
          subs {
            ...AmeliSubFragment
          }
        }
      }
    |js}
    ++ ameliAmdFragment
    ++ ameliAviFragment
    ++ ameliEntFragment
    ++ ameliGrpPolFragment
    ++ ameliNatFragment
    ++ ameliSesFragment
    ++ ameliSorFragment
    ++ ameliSubFragment
    ++ ameliTxtFragment
    ++ ameliTypSubFragment,
  );

let createSenatDiscussionQuery:
  (string, string, string) => ApolloClient.queryObj = [%bs.raw
  {|
function (num, sesinsLil, txttyp) {
  return {
    query: senatDiscussionQuery,
    variables: {
      num: num,
      sesinsLil: sesinsLil,
      txttyp: txttyp
    }
  };
}
  |}
];

let deleteGraphqlClient: graphqlClient => unit = [%bs.raw
  {|
function (graphqlClient) {
  // TODO: Is there something to do?
}
  |}
];
