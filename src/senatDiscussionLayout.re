open Configuration;

open Model;

open Types;

[%bs.raw {|require('./discussionLayout.css')|}];

type action =
  | ToggleShowMobileDrawer;

type state = {
  showMobileDrawer: bool,
  todo: int,
};

let drawer = (txt: ameliTxt) => {
  let urlPath = urlPathFromSenatDiscussion(txt);
  <div className="drawer-paper-div">
    <MaterialUI.AppBar position="static" color="default">
      <MaterialUI.Toolbar>
        <MaterialUI.Typography
          variant=MaterialUI.Typography.Variant.Title
          color=MaterialUI.Typography.Color.Inherit>
          (config.appTitle |> ste)
        </MaterialUI.Typography>
      </MaterialUI.Toolbar>
    </MaterialUI.AppBar>
    <MaterialUI.List component="nav">
      <MaterialUI.ListItem
        button=true
        href=urlPath
        onClick=(
          event => {
            ReactEventRe.Mouse.preventDefault(event);
            ReactEventRe.Mouse.stopPropagation(event);
            ReasonReact.Router.push(urlPath);
          }
        )>
        <MaterialUI.ListItemIcon>
          <MaterialUIIcons.Inbox />
        </MaterialUI.ListItemIcon>
        <MaterialUI.ListItemText
          primary="Index chronologique des amendements"
        />
      </MaterialUI.ListItem>
      <MaterialUI.ListItem
        button=true
        href=(urlPath ++ "/groupes")
        onClick=(
          event => {
            ReactEventRe.Mouse.preventDefault(event);
            ReactEventRe.Mouse.stopPropagation(event);
            ReasonReact.Router.push(urlPath ++ "/groupes");
          }
        )>
        <MaterialUI.ListItemIcon>
          <MaterialUIIcons.Inbox />
        </MaterialUI.ListItemIcon>
        <MaterialUI.ListItemText primary="Index des amendements par auteur" />
      </MaterialUI.ListItem>
      <MaterialUI.ListItem
        button=true
        href=(urlPath ++ "/tout")
        onClick=(
          event => {
            ReactEventRe.Mouse.preventDefault(event);
            ReactEventRe.Mouse.stopPropagation(event);
            ReasonReact.Router.push(urlPath ++ "/tout");
          }
        )>
        <MaterialUI.ListItemIcon>
          <MaterialUIIcons.Inbox />
        </MaterialUI.ListItemIcon>
        <MaterialUI.ListItemText primary="Tous les amendements" />
      </MaterialUI.ListItem>
    </MaterialUI.List>
  </div>;
};

let component = ReasonReact.reducerComponent("SenatDiscussionLayout");

let make = (~onNavigateToAmendement, children, ~txt: ameliTxt) => {
  ...component,
  initialState: () => {showMobileDrawer: false, todo: 0},
  reducer: (action, state) =>
    switch (action) {
    | ToggleShowMobileDrawer =>
      ReasonReact.Update({
        ...state,
        showMobileDrawer: ! state.showMobileDrawer,
      })
    },
  render: self =>
    <div
      style=(
        ReactDOMRe.Style.make(
          /* ~display="flex", */
          /* ~height="100%", */
          ~position="relative",
          ~width="100%",
          (),
        )
      )>
      <MaterialUI.AppBar className="app-bar" position="static">
        <MaterialUI.Toolbar
          style=(
            ReactDOMRe.Style.make(
              ~justifyContent="space-between",
              ~display="flex",
              (),
            )
          )>
          <div
            style=(
              ReactDOMRe.Style.make(~alignItems="center", ~display="flex", ())
            )>
            <MaterialUI.IconButton
              className="menu-button"
              color=MaterialUI.IconButton.Color.Inherit
              onClick=(_event => self.send(ToggleShowMobileDrawer))>
              <MaterialUIIcons.Menu />
            </MaterialUI.IconButton>
            <MaterialUI.Typography
              color=MaterialUI.Typography.Color.Inherit
              variant=MaterialUI.Typography.Variant.Title>
              (txt.int |> ste)
            </MaterialUI.Typography>
          </div>
        </MaterialUI.Toolbar>
      </MaterialUI.AppBar>
      /* <div>
           (
             switch txt.numeroProchainADiscuter {
             | None => ReasonReact.nullElement
             | Some(numeroProchainADiscuter) =>
               let amendementUrlPath =
                 urlPathFromSenatDiscussion(txt)
                 ++ "/tout#"
                 ++ Slugify.slugify(numeroProchainADiscuter);
               <MaterialUI.IconButton
                 href=amendementUrlPath
                 onClick=(
                   event => {
                     ReactEventRe.Mouse.preventDefault(event);
                     ReactEventRe.Mouse.stopPropagation(event);
                     onNavigateToAmendement(numeroProchainADiscuter);
                   }
                 )
                 style=(ReactDOMRe.Style.make(~color="orange", ()))>
                 <MaterialUIIcons.Whatshot />
               </MaterialUI.IconButton>;
             }
           )
           (
             if (txt.bibard == "490" || txt.bibard == "592") {
               <MaterialUI.Button
                 color=MaterialUI.Button.Color.Inherit
                 href="https://donnees-personnelles.parlement-ouvert.fr/"
                 title={js|Accéder au dossier législatif.|js}>
                 {js|Dossier législatif|js}
               </MaterialUI.Button>;
             } else {
               ReasonReact.nullElement;
             }
           )
         </div> */
      <MaterialUI.Hidden mdUp=true>
        <MaterialUI.Drawer
          anchor=MaterialUI.Drawer.Anchor.Left
          className="drawer-paper"
          onClose=(_event => self.send(ToggleShowMobileDrawer))
          _open=self.state.showMobileDrawer
          /* modalProps={.
               "keepMounted": Js.Boolean.to_js_boolean(true) /* Better open performance on mobile. */
             } */
          variant=MaterialUI.Drawer.Variant.Temporary>
          (drawer(txt))
        </MaterialUI.Drawer>
      </MaterialUI.Hidden>
      <MaterialUI.Hidden
        implementation=MaterialUI.Hidden.Implementation.Css smDown=true>
        <MaterialUI.Drawer
          className="drawer-paper"
          _open=true
          variant=MaterialUI.Drawer.Variant.Permanent>
          (drawer(txt))
        </MaterialUI.Drawer>
      </MaterialUI.Hidden>
      /* This doesn't work yet: See: https://reasonml.github.io/reason-react/docs/en/children.html.GraphqlSubscription
         <main className="content">...children</main> */
      (
        ReasonReact.createDomElement(
          "main",
          ~props={"className": "content"},
          children,
        )
      )
    </div>,
};
