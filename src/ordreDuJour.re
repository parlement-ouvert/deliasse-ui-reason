open Configuration;

open Model;

open Types;

let component = ReasonReact.statelessComponent("OrdreDuJour");

let make =
    (~legislature, ~organe, ~ordreDuJour: array(ordreDuJourItem), _children) => {
  ...component,
  render: _self =>
    <div>
      <MaterialUI.AppBar position="static">
        <MaterialUI.Toolbar>
          <MaterialUI.Typography
            color=MaterialUI.Typography.Color.Inherit
            variant=MaterialUI.Typography.Variant.Title>
            (config.appTitle |> ste)
          </MaterialUI.Typography>
        </MaterialUI.Toolbar>
      </MaterialUI.AppBar>
      <MaterialUI.Typography
        color=MaterialUI.Typography.Color.Inherit
        component="h3"
        variant=MaterialUI.Typography.Variant.Display1>
        (organe |> ste)
      </MaterialUI.Typography>
      <MaterialUI.Typography
        color=MaterialUI.Typography.Color.Inherit
        component="h4"
        variant=MaterialUI.Typography.Variant.Title>
        ({js|Ordre du jour|js} |> ste)
      </MaterialUI.Typography>
      <ul>
        (
          ordreDuJour
          |> Array.map(ordreDuJourItem => {
               let urlPath =
                 assembleeDiscussionUrlPath(
                   ~bibard=ordreDuJourItem.textBibard,
                   ~bibardSuffixe=ordreDuJourItem.textBibardSuffixe,
                   ~legislature=int_of_string(legislature),
                   ~organe,
                 );
               <li
                 key=(
                   ordreDuJourItem.textBibard
                   ++ ordreDuJourItem.textBibardSuffixe
                 )>
                 <a
                   href=urlPath
                   onClick=(
                     event => {
                       ReactEventRe.Mouse.preventDefault(event);
                       ReactEventRe.Mouse.stopPropagation(event);
                       ReasonReact.Router.push(urlPath);
                     }
                   )>
                   (ordreDuJourItem.textTitre |> ste)
                 </a>
               </li>;
             })
          |> ReasonReact.arrayToElement
        )
      </ul>
    </div>,
};
