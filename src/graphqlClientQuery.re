open Configuration;

open Model;

type action =
  | GotGraphqlClient(Apollo.graphqlClient);

type page =
  | AssembleeDiscussionPage(
      string,
      string,
      string,
      string,
      list(string),
      string,
    )
  | AssembleeOrganePage(string, string)
  | AssembleeOrganesPage(string)
  | HomePage
  | NotFoundPage(list(string))
  | ObsoletePage
  | SenatDiscussionPage(string, string, string, list(string), string);

type state = {
  graphqlClient: option(Apollo.graphqlClient),
  page,
};

let pathToPage = (path: list(string), hash: string) : page =>
  switch (path) {
  | [] => HomePage
  | ["assemblee", "discussions", legislature] =>
    AssembleeOrganesPage(legislature)
  | ["assemblee", "discussions", legislature, organe] =>
    AssembleeOrganePage(legislature, organe)
  | ["assemblee", "discussions", legislature, organe, bibard] =>
    AssembleeDiscussionPage(legislature, organe, bibard, "", [], hash)
  | [
      "assemblee",
      "discussions",
      legislature,
      organe,
      bibard,
      bibardSuffixe,
      ...rest,
    ] =>
    AssembleeDiscussionPage(
      legislature,
      organe,
      bibard,
      bibardSuffixe,
      rest,
      hash,
    )
  | ["discussions", ..._rest] => ObsoletePage
  | ["senat", "discussions", txttyp, sesinsLil, num, ...rest] =>
    SenatDiscussionPage(txttyp, sesinsLil, num, rest, hash)
  | _ => NotFoundPage(path)
  };

let component = ReasonReact.reducerComponent("GraphqlClientQuery");

let make = (~hash, ~path, _children) => {
  ...component,
  initialState: () => {graphqlClient: None, page: pathToPage(path, hash)},
  subscriptions: self => [
    Sub(
      () => {
        let graphqlClient =
          Apollo.createGraphqlClient(
            Urls.resolve(config.apiUrl, "/graphql"),
            "ws"
            ++ Js.String.sliceToEnd(
                 ~from=4,
                 Urls.resolve(config.apiUrl, "/subscriptions"),
               ),
          );
        self.send(GotGraphqlClient(graphqlClient));
        graphqlClient;
      },
      Apollo.deleteGraphqlClient,
    ),
  ],
  willReceiveProps: ({state}) => {
    let page = pathToPage(path, hash);
    if (shallowEqual(asJsObject(page), asJsObject(state.page))) {
      state;
    } else {
      {...state, page};
    };
  },
  reducer: (action, state) =>
    switch (action) {
    | GotGraphqlClient(graphqlClient) =>
      ReasonReact.Update({...state, graphqlClient: Some(graphqlClient)})
    },
  render: self =>
    switch (self.state.graphqlClient) {
    | None =>
      <div>
        <MaterialUI.AppBar position="static">
          <MaterialUI.Toolbar>
            <MaterialUI.Typography
              color=MaterialUI.Typography.Color.Inherit
              variant=MaterialUI.Typography.Variant.Title>
              (config.appTitle |> ste)
            </MaterialUI.Typography>
          </MaterialUI.Toolbar>
        </MaterialUI.AppBar>
        <MaterialUI.LinearProgress />
      </div>
    | Some(graphqlClient) =>
      switch (self.state.page) {
      | AssembleeDiscussionPage(
          legislature,
          organe,
          bibard,
          bibardSuffixe,
          path,
          hash,
        ) =>
        <AssembleeDiscussionQuery
          bibard
          bibardSuffixe
          graphqlClient
          hash
          legislature
          organe
          path
        />
      | AssembleeOrganePage(legislature, organe) =>
        <OrdreDuJourQuery graphqlClient legislature organe />
      | AssembleeOrganesPage(legislature) =>
        <OrganesQuery graphqlClient legislature />
      | HomePage =>
        ReasonReact.Router.push("/assemblee/discussions/15/AN/592//tout");
        <div>
          <MaterialUI.AppBar position="static">
            <MaterialUI.Toolbar>
              <MaterialUI.Typography
                color=MaterialUI.Typography.Color.Inherit
                variant=MaterialUI.Typography.Variant.Title>
                (config.appTitle |> ste)
              </MaterialUI.Typography>
            </MaterialUI.Toolbar>
          </MaterialUI.AppBar>
          <MaterialUI.LinearProgress />
        </div>;
      | NotFoundPage(_path) =>
        <div>
          <MaterialUI.AppBar position="static">
            <MaterialUI.Toolbar>
              <MaterialUI.Typography
                color=MaterialUI.Typography.Color.Inherit
                variant=MaterialUI.Typography.Variant.Title>
                (config.appTitle |> ste)
              </MaterialUI.Typography>
            </MaterialUI.Toolbar>
          </MaterialUI.AppBar>
          <p> ("Page not foud!" |> ste) </p>
        </div>
      | ObsoletePage =>
        /* Obsolete page => Redirect to home page. */
        ReasonReact.Router.push("/");
        <div>
          <MaterialUI.AppBar position="static">
            <MaterialUI.Toolbar>
              <MaterialUI.Typography
                color=MaterialUI.Typography.Color.Inherit
                variant=MaterialUI.Typography.Variant.Title>
                (config.appTitle |> ste)
              </MaterialUI.Typography>
            </MaterialUI.Toolbar>
          </MaterialUI.AppBar>
          <MaterialUI.LinearProgress />
        </div>;
      | SenatDiscussionPage(txttyp, sesinsLil, num, path, hash) =>
        <SenatDiscussionQuery graphqlClient hash num path sesinsLil txttyp />
      }
    },
};
