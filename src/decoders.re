open Types;

let etat = (json: Js.Json.t) : etat =>
  Json.Decode.(
    json
    |> string
    |> (
      str =>
        switch (str) {
        | "AC" => Accepte
        | "DI" => Discute
        | somethingElse =>
          raise @@ DecodeError("Unknown etat " ++ somethingElse)
        }
    )
  );

let groupe = (json: Js.Json.t) : groupe =>
  Json.Decode.(
    json
    |> string
    |> (
      str =>
        switch (str) {
        | "FI" => LaFranceInsoumise
        | "GDR" => GaucheDemocrateEtRepublicaine
        | "LR" => LesRepublicains
        | "MODEM" => MouvementDemocrateEtApparentes
        | "NG" => NouvelleGauche
        | "NI" => NonInscrits
        | "REM" => LaRepubliqueEnMarche
        | "UDI-AGIR" => UdiAgirEtIndependants
        | somethingElse =>
          raise @@ DecodeError("Unknown groupe " ++ somethingElse)
        }
    )
  );

let sortEnSeance = (json: Js.Json.t) : sortEnSeance =>
  Json.Decode.(
    json
    |> string
    |> (
      str => {
        /* With current version of Bucklescript, switch statement doesn't work with diacritical characters. */
        let slug = Slugify.slugify(str);
        switch (slug) {
        | "" => Depose
        | "adopte" => Adopte
        | "non-soutenu" => NonSoutenu
        | "rejete" => Rejete
        | "retire" => Retire
        | "tombe" => Tombe
        | somethingElse =>
          raise @@ DecodeError("Unknown sortEnSeance " ++ somethingElse)
        };
      }
    )
  );

let typeDiscussion = (json: Js.Json.t) : typeDiscussion =>
  Json.Decode.(
    json
    |> string
    |> (
      str =>
        switch (str) {
        | "projet de loi" => ProjetDeLoi
        | somethingElse =>
          raise @@ DecodeError("Unknown typeDiscussion " ++ somethingElse)
        }
    )
  );

let ameliAvi = (json: Js.Json.t) : ameliAvi =>
  Json.Decode.{
    id: json |> field("id", string),
    lib: json |> field("lib", string),
    cod: json |> field("cod", string),
  };

let ameliCab = (json: Js.Json.t) : ameliCab =>
  Json.Decode.{
    id: json |> field("id", int),
    typ: json |> field("typ", string),
    act: json |> field("act", string),
    entid: json |> field("entid", int),
    codint: json |> field("codint", string),
    lilOptional: json |> field("lilOptional", optional(string)),
  };

let ameliCom = (json: Js.Json.t) : ameliCom =>
  Json.Decode.{
    id: json |> field("id", int),
    typ: json |> field("typ", string),
    act: json |> field("act", string),
    entid: json |> field("entid", int),
    cod: json |> field("cod", string),
    lib: json |> field("lib", string),
    lil: json |> field("lil", string),
    spc: json |> field("spc", string),
    codint: json |> field("codint", string),
    tri: json |> field("tri", optional(int)),
  };

let ameliGrpPol = (json: Js.Json.t) : ameliGrpPol =>
  Json.Decode.{
    id: json |> field("id", int),
    typ: json |> field("typ", string),
    act: json |> field("act", string),
    entid: json |> field("entid", int),
    cod: json |> field("cod", string),
    libcou: json |> field("libcou", string),
    lilcou: json |> field("lilcou", string),
    codint: json |> field("codint", string),
    tri: json |> field("tri", optional(int)),
  };

let ameliGvt = (json: Js.Json.t) : ameliGvt =>
  Json.Decode.{
    id: json |> field("id", int),
    typ: json |> field("typ", string),
    act: json |> field("act", string),
    entid: json |> field("entid", int),
    nom: json |> field("nom", string),
    pre: json |> field("pre", string),
    qua: json |> field("qua", string),
    tit: json |> field("tit", string),
  };

let ameliSen = (json: Js.Json.t) : ameliSen =>
  Json.Decode.{
    id: json |> field("id", int),
    typ: json |> field("typ", string),
    act: json |> field("act", string),
    entid: json |> field("entid", int),
    grp: json |> field("grp", ameliGrpPol),
    grpid: json |> field("grpid", int),
    comid: json |> field("comid", optional(int)),
    comspcid: json |> field("comspcid", optional(int)),
    mat: json |> field("mat", string),
    qua: json |> field("qua", string),
    nomuse: json |> field("nomuse", string),
    prenomuse: json |> field("prenomuse", string),
    nomtec: json |> field("nomtec", optional(string)),
    hom: json |> field("hom", optional(string)),
    app: json |> field("app", optional(string)),
    ratt: json |> field("ratt", optional(string)),
    nomusemin: json |> field("nomusemin", string),
    senfem: json |> field("senfem", optional(string)),
  };

let ameliEnt = (json: Js.Json.t) : ameliEnt =>
  switch (Json.Decode.(field("typ", string, json))) {
  | "B" => AmeliCab(ameliCab(json))
  | "C" => AmeliCom(ameliCom(json))
  | "E" => AmeliSen(ameliSen(json))
  | "G" => AmeliGrpPol(ameliGrpPol(json))
  | "M" => AmeliGvt(ameliGvt(json))
  | somethingElse =>
    raise @@ Json.Decode.DecodeError("Unknown ameliEnt.typ " ++ somethingElse)
  };

let ameliSor = (json: Js.Json.t) : ameliSor =>
  Json.Decode.{
    id: json |> field("id", string),
    lib: json |> field("lib", string),
    cod: json |> field("cod", string),
    typ: json |> field("typ", string),
  };

let ameliTypSub = (json: Js.Json.t) : ameliTypSub =>
  Json.Decode.{
    id: json |> field("id", int),
    lib: json |> field("lib", string),
  };

let ameliSub = (json: Js.Json.t) : ameliSub =>
  Json.Decode.{
    id: json |> field("id", int),
    txtid: json |> field("txtid", int),
    merid: json |> field("merid", optional(int)),
    typ: json |> field("typ", optional(ameliTypSub)),
    typid: json |> field("typid", optional(int)),
    lic: json |> field("lic", optional(string)),
    lib: json |> field("lib", optional(string)),
    pos: json |> field("pos", optional(int)),
    sig_: json |> field("sig", optional(string)),
    posder: json |> field("posder", optional(int)),
    prires: json |> field("prires", optional(int)),
    dupl: json |> field("dupl", string),
    subamd: json |> field("subamd", string),
    sorid: json |> field("sorid", optional(string)),
    txtidder: json |> field("txtidder", optional(int)),
    style: json |> field("style", string),
  };

let ameliAmd = (json: Js.Json.t) : ameliAmd =>
  Json.Decode.{
    id: json |> field("id", int),
    sub: json |> field("sub", optional(ameliSub)),
    subid: json |> field("subid", optional(int)),
    amdperid: json |> field("amdperid", optional(int)),
    motid: json |> field("motid", optional(int)),
    etaid: json |> field("etaid", int),
    noment: json |> field("noment", optional(ameliEnt)),
    nomentid: json |> field("nomentid", int),
    sor: json |> field("sor", optional(ameliSor)),
    sorid: json |> field("sorid", optional(string)),
    avc: json |> field("avc", optional(ameliAvi)),
    avcid: json |> field("avcid", optional(string)),
    avg: json |> field("avg", optional(ameliAvi)),
    avgid: json |> field("avgid", optional(string)),
    irrid: json |> field("irrid", optional(int)),
    /* txt: json |> field("txt", ameliTxt), */
    txtid: json |> field("txtid", int),
    opmid: json |> field("opmid", optional(int)),
    ocmid: json |> field("ocmid", optional(int)),
    ideid: json |> field("ideid", optional(int)),
    discomid: json |> field("discomid", optional(int)),
    num: json |> field("num", optional(string)),
    rev: json |> field("rev", int),
    typ: json |> field("typ", string),
    dis: json |> field("dis", optional(string)),
    obj: json |> field("obj", optional(string)),
    datdep: json |> field("datdep", optional(string)),
    obs: json |> field("obs", optional(string)),
    ord: json |> field("ord", optional(int)),
    autext: json |> field("autext", bool),
    subpos: json |> field("subpos", optional(int)),
    mot: json |> field("mot", optional(string)),
    numabs: json |> field("numabs", optional(int)),
    subidder: json |> field("subidder", optional(int)),
    libgrp: json |> field("libgrp", optional(string)),
    alinea: json |> field("alinea", optional(int)),
    accgou: json |> field("accgou", bool),
    colleg: json |> field("colleg", bool),
    typrectid: json |> field("typrectid", optional(int)),
    islu: json |> field("islu", bool),
    motposexa: json |> field("motposexa", string),
  };

let ameliNat = (json: Js.Json.t) : ameliNat =>
  Json.Decode.{
    id: json |> field("id", int),
    lib: json |> field("lib", string),
  };

let ameliSes = (json: Js.Json.t) : ameliSes =>
  Json.Decode.{
    id: json |> field("id", int),
    typid: json |> field("typid", int),
    ann: json |> field("ann", int),
    lil: json |> field("lil", string),
  };

let ameliTxt = (json: Js.Json.t) : ameliTxt =>
  Json.Decode.{
    id: json |> field("id", int),
    nat: json |> field("nat", ameliNat),
    natid: json |> field("natid", int),
    lecid: json |> field("natid", int),
    sesins: json |> field("sesins", optional(ameliSes)),
    sesinsid: json |> field("sesinsid", optional(int)),
    sesdep: json |> field("sesdep", ameliSes),
    sesdepid: json |> field("sesdepid", int),
    fbuid: json |> field("fbuid", optional(int)),
    num: json |> field("num", string),
    int: json |> field("int", string),
    inl: json |> field("inl", optional(string)),
    datdep: json |> field("datdep", string),
    urg: json |> field("urg", string),
    dis: json |> field("dis", string),
    secdel: json |> field("secdel", string),
    loifin: json |> field("loifin", string),
    loifinpar: json |> field("loifinpar", optional(int)),
    txtamd: json |> field("txtamd", string),
    datado: json |> field("datado", optional(string)),
    numado: json |> field("numado", optional(int)),
    txtexa: json |> field("txtexa", optional(string)),
    pubdellim: json |> field("pubdellim", optional(string)),
    numabs: json |> field("numabs", optional(int)),
    libdelim: json |> field("libdelim", optional(string)),
    libcplnat: json |> field("libcplnat", optional(string)),
    doslegsignet: json |> field("doslegsignet", optional(string)),
    proacc: json |> field("proacc", string),
    txttyp: json |> field("txttyp", string),
    ordsnddelib: json |> field("ordsnddelib", optional(string)),
    txtetaid: json |> field("txtetaid", int),
    fusderid: json |> field("fusderid", optional(int)),
    fusder: json |> field("fusder", string),
    fusderord: json |> field("fusderord", int),
    fusdertyp: json |> field("fusdertyp", optional(string)),
    amds: json |> field("amds", array(ameliAmd)),
    subs: json |> field("subs", array(ameliSub)),
  };

let auteur = (json: Js.Json.t) : auteur =>
  Json.Decode.{
    civilite: json |> field("civilite", string),
    estRapporteur: json |> field("estRapporteur", bool),
    estGouvernement: json |> field("estGouvernement", bool),
    groupeTribunId: json |> field("groupeTribunId", string),
    nom: json |> field("nom", string),
    photoUrl: json |> field("photoUrl", string),
    prenom: json |> field("prenom", string),
    qualite: json |> field("qualite", optional(string)),
    tribunId: json |> field("tribunId", string),
  };

let cosignatairesMentionLibre = (json: Js.Json.t) : cosignatairesMentionLibre =>
  Json.Decode.{titre: json |> field("string", string)};

let amendement = (json: Js.Json.t) : amendement =>
  Json.Decode.{
    auteur: json |> field("auteur", auteur),
    bibard: json |> field("bibard", string),
    bibardSuffixe: json |> field("bibardSuffixe", string),
    cosignataires: json |> field("cosignataires", array(auteur)),
    cosignatairesMentionLibre:
      json
      |> field(
           "cosignatairesMentionLibre",
           optional(cosignatairesMentionLibre),
         ),
    dispositif: json |> field("dispositif", optional(string)),
    etat: json |> field("etat", etat),
    exposeSommaire: json |> field("exposeSommaire", optional(string)),
    legislature: json |> field("legislature", int),
    numero: json |> field("numero", string),
    numeroLong: json |> field("numeroLong", string),
    numeroReference: json |> field("numeroReference", string),
    organeAbrv: json |> field("organeAbrv", string),
    place: json |> field("place", string),
    placeReference: json |> field("placeReference", string),
    sortEnSeance: json |> field("sortEnSeance", sortEnSeance),
  };

let amendementUpsertedWrapper = (json: Js.Json.t) : amendement =>
  Json.Decode.(json |> field("amendementUpserted", amendement));

let amendementWrapper = (json: Js.Json.t) : amendementWrapper =>
  Json.Decode.{
    alineaLabel: json |> field("alineaLabel", string),
    amendement: json |> field("amendement", optional(amendement)),
    auteurGroupe: json |> field("auteurGroupe", optional(groupe)),
    auteurLabel: json |> field("auteurLabel", string),
    discussionCommune: json |> field("discussionCommune", string),
    discussionCommuneAmdtPositon:
      json |> field("discussionCommuneAmdtPositon", string),
    discussionCommuneSsAmdtPositon:
      json |> field("discussionCommuneSsAmdtPositon", string),
    discussionIdentique: json |> field("discussionIdentique", string),
    discussionIdentiqueAmdtPositon:
      json |> field("discussionIdentiqueAmdtPositon", string),
    discussionIdentiqueSsAmdtPositon:
      json |> field("discussionIdentiqueSsAmdtPositon", string),
    missionLabel: json |> field("missionLabel", string),
    numero: json |> field("numero", string),
    parentNumero: json |> field("parentNumero", string),
    place: json |> field("place", string),
    position: json |> field("position", string),
    sort: json |> field("sort", sortEnSeance),
  };

let billPage = (json: Js.Json.t) : billPage =>
  Json.Decode.{
    body: json |> field("body", string),
    numero: json |> field("numero", string),
    slug: json |> field("slug", string),
    titre: json |> field("titre", optional(string)),
  };

let config = (json: Js.Json.t) : config =>
  Json.Decode.{
    apiUrl: json |> field("apiUrl", string),
    appTitle: json |> field("appTitle", string),
    appUrl: json |> field("appUrl", string),
    twitterName: json |> field("twitterName", string),
  };

let division = (json: Js.Json.t) : division =>
  Json.Decode.{
    /* amendements: json |> field("amendements", array(amendementWrapper)) */
    place: json |> field("place", string),
    position: json |> field("position", string),
  };

let discussion = (json: Js.Json.t) : discussion =>
  Json.Decode.{
    amendements: json |> field("amendements", array(amendementWrapper)),
    bibard: json |> field("bibard", string),
    bibardSuffixe: json |> field("bibardSuffixe", string),
    divisions: json |> field("divisions", array(division)),
    legislature: json |> field("legislature", int),
    numeroProchainADiscuter:
      json |> field("numeroProchainADiscuter", optional(string)),
    organe: json |> field("organe", string),
    pages:
      json
      |> field("pages", array(billPage))
      |> (
        billPages => {
          let billPageBySlug =
            Hashtbl.create(~random=true, Array.length(billPages));
          billPages
          |> Array.iter((billPage: billPage) =>
               Hashtbl.add(billPageBySlug, billPage.slug, billPage)
             );
          billPageBySlug;
        }
      ),
    titre: json |> field("titre", string),
    typeDiscussion: json |> field("type", typeDiscussion),
  };

let discussionWrapper = (json: Js.Json.t) : discussion =>
  Json.Decode.(json |> field("discussion", discussion));

let ordreDuJourItem = (json: Js.Json.t) : ordreDuJourItem =>
  Json.Decode.{
    textBibard: json |> field("textBibard", string),
    textBibardSuffixe: json |> field("textBibardSuffixe", string),
    textTitre: json |> field("textTitre", string),
  };

let ordreDuJourWrapper = (json: Js.Json.t) : array(ordreDuJourItem) =>
  Json.Decode.(json |> field("ordreDuJour", array(ordreDuJourItem)));

let prochainADiscuter = (json: Js.Json.t) : prochainADiscuter =>
  Json.Decode.{
    bibard: json |> field("bibard", string),
    bibardSuffixe: json |> field("bibardSuffixe", string),
    legislature: json |> field("legislature", int),
    nbrAmdtRestant: json |> field("nbrAmdtRestant", int),
    numAmdt: json |> field("numAmdt", string),
    organeAbrv: json |> field("organeAbrv", string),
  };

let prochainADiscuterUpsertedWrapper = (json: Js.Json.t) : prochainADiscuter =>
  Json.Decode.(json |> field("prochainADiscuterUpserted", prochainADiscuter));

let referenceOrgane = (json: Js.Json.t) : referenceOrgane =>
  Json.Decode.{
    text: json |> field("text", string),
    value: json |> field("value", string),
  };

let referencesOrganesWrapper = (json: Js.Json.t) : array(referenceOrgane) =>
  Json.Decode.(json |> field("referencesOrganes", array(referenceOrgane)));

let senatDiscussionWrapper = (json: Js.Json.t) : ameliTxt =>
  Json.Decode.(json |> field("senatDiscussion", ameliTxt));
