open Model;

open Types;

let component = ReasonReact.statelessComponent("AssembleeSortEnSeance");

let make = (~sortEnSeance: sortEnSeance, _children) => {
  ...component,
  render: _self =>
    <mark
      style=(
        ReactDOMRe.Style.make(
          ~backgroundColor=
            switch (sortEnSeance) {
            | Adopte => "#4caf50"
            | Depose => "grey"
            | _ => "#d0021b"
            },
          ~color="white",
          ~flex="1 1 auto",
          ~fontSize="16px",
          ~marginLeft="10px",
          ~padding="5px 5px 5px 5px",
          (),
        )
      )>
      (
        (
          switch (sortEnSeance) {
          | Adopte => {js|Adopté|js}
          | Depose => {js|À discuter|js}
          | NonSoutenu => {js|Non soutenu|js}
          | Rejete => {js|Rejeté|js}
          | Retire => {js|Retiré|js}
          | Tombe => {js|Tombé|js}
          }
        )
        |> ste
      )
    </mark>,
};
