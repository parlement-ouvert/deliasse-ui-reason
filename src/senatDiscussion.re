open Configuration;

open Model;

open Types;

type page =
  | ByGroupePage
  | FullPage
  | IndexPage
  | NotFoundPage(list(string));

type action =
  | CloseSnackbar
  /* | GotAmendement(Js.Json.t)
     | GotProchainADiscuter(Js.Json.t) */
  | ScrollToHash;

type state = {
  hash: string,
  openSnackbar: bool,
  page,
  path: list(string),
  txt: ameliTxt,
};

let component = ReasonReact.reducerComponent("SenatDiscussion");

let pathToPage = path : page =>
  switch (path) {
  | [] => IndexPage
  | ["groupes"] => ByGroupePage
  | ["tout"] => FullPage
  | _ => NotFoundPage(path)
  };

let make = (~graphqlClient as _graphqlClient, ~hash, ~path, ~txt, _children) => {
  ...component,
  initialState: () => {
    hash,
    openSnackbar: false,
    page: pathToPage(path),
    path,
    txt,
  },
  /* subscriptions: self => [
       Sub(
         () =>
           GraphqlSubscription.subscribe(
             ~graphqlClient,
             ~query=Apollo.createAmendementUpsertedSubscriptionQuery(),
             ~receiver=data =>
             self.send(GotAmendement(data))
           ),
         GraphqlSubscription.unsubscribe
       ),
       Sub(
         () =>
           GraphqlSubscription.subscribe(
             ~graphqlClient,
             ~query=Apollo.createProchainADiscuterUpsertedSubscriptionQuery(),
             ~receiver=data =>
             self.send(GotProchainADiscuter(data))
           ),
         GraphqlSubscription.unsubscribe
       )
     ], */
  didMount: ({send}) => {
    send(ScrollToHash);
    ReasonReact.NoUpdate;
  },
  didUpdate: ({newSelf}) => newSelf.send(ScrollToHash),
  willReceiveProps: ({state}) =>
    if (shallowEqual(asJsObject(path), asJsObject(state.path))
        && hash === state.hash) {
      state;
    } else {
      let page = pathToPage(path);
      {...state, hash, page, path};
    },
  reducer: (action, state) =>
    switch (action) {
    | CloseSnackbar => ReasonReact.Update({...state, openSnackbar: false})
    /* | GotAmendement(data) =>
         /* Js.log2("GotAmendement", data); */
         let discussion = state.discussion;
         let amendement = Decoders.amendementUpsertedWrapper(data);
         if (amendement.bibard == discussion.bibard
             && amendement.bibardSuffixe == discussion.bibardSuffixe
             && amendement.legislature == discussion.legislature
             && amendement.organeAbrv == discussion.organe) {
           let amendementsWrappers =
             discussion.amendements
             |> Array.map((amendementWrapper: amendementWrapper) =>
                  if (amendementWrapper.numero == amendement.numeroReference) {
                    {
                      ...amendementWrapper,
                      amendement: Some(amendement),
                      sort: amendement.sortEnSeance
                    };
                  } else {
                    amendementWrapper;
                  }
                );
           ReasonReact.Update({
             ...state,
             discussion: {
               ...discussion,
               amendements: amendementsWrappers
             }
           });
         } else {
           ReasonReact.NoUpdate;
         };
       | GotProchainADiscuter(data) =>
         /* Js.log2("GotProchainADiscuter", data); */
         let discussion = state.discussion;
         let prochainADiscuter = Decoders.prochainADiscuterUpsertedWrapper(data);
         if (prochainADiscuter.bibard == discussion.bibard
             && prochainADiscuter.bibardSuffixe == discussion.bibardSuffixe
             && prochainADiscuter.legislature == discussion.legislature
             && prochainADiscuter.organeAbrv == discussion.organe) {
           ReasonReact.Update({
             ...state,
             discussion: {
               ...discussion,
               numeroProchainADiscuter: Some(prochainADiscuter.numAmdt)
             },
             openSnackbar: true
           });
         } else {
           ReasonReact.NoUpdate;
         }; */
    | ScrollToHash =>
      state.hash === "" ?
        ReasonReact.NoUpdate :
        ReasonReact.SideEffects((_self => Scroller.scrollToId(state.hash)))
    },
  render: self =>
    <div>
      (
        switch (self.state.page) {
        | ByGroupePage =>
          <SenatDiscussionByGroupe
            onNavigateToAmendement=(
              (numero: string) => {
                let hash = Slugify.slugify(numero);
                let amendementUrlPath =
                  urlPathFromSenatDiscussion(self.state.txt)
                  ++ "/tout#"
                  ++ hash;
                ReasonReact.Router.push(amendementUrlPath);
              }
            )
            txt=self.state.txt
          />
        | FullPage =>
          <SenatDiscussionFull
            onNavigateToAmendement=(
              (numero: string) => {
                let hash = Slugify.slugify(numero);
                let amendementUrlPath =
                  urlPathFromSenatDiscussion(self.state.txt)
                  ++ "/tout#"
                  ++ hash;
                ReasonReact.Router.push(amendementUrlPath);
              }
            )
            txt=self.state.txt
          />
        | IndexPage =>
          <SenatDiscussionIndex
            onNavigateToAmendement=(
              (numero: string) => {
                let hash = Slugify.slugify(numero);
                let amendementUrlPath =
                  urlPathFromSenatDiscussion(self.state.txt)
                  ++ "/tout#"
                  ++ hash;
                ReasonReact.Router.push(amendementUrlPath);
              }
            )
            txt=self.state.txt
          />
        | NotFoundPage(_path) =>
          <div>
            <MaterialUI.AppBar position="static">
              <MaterialUI.Toolbar>
                <MaterialUI.Typography
                  color=MaterialUI.Typography.Color.Inherit
                  variant=MaterialUI.Typography.Variant.Title>
                  (config.appTitle |> ste)
                </MaterialUI.Typography>
              </MaterialUI.Toolbar>
            </MaterialUI.AppBar>
            <p> ("Page not foud!" |> ste) </p>
          </div>
        }
      )
    </div>,
  /* {
       let txt = self.state.txt;
       switch (discussion.numeroProchainADiscuter, self.state.openSnackbar) {
       | (Some(numeroProchainADiscuter), true) =>
         let hash = Slugify.slugify(numeroProchainADiscuter);
         let amendementUrlPath =
           urlPathFromSenatDiscussion(discussion) ++ "/tout#" ++ hash;
         <MaterialUI.Snackbar
           action=
             <span>
               <MaterialUI.Button
                 color=MaterialUI.Button.Color.Secondary
                 size=MaterialUI.Button.Size.Small
                 href=amendementUrlPath
                 onClick=(
                   event => {
                     ReactEventRe.Mouse.preventDefault(event);
                     ReactEventRe.Mouse.stopPropagation(event);
                     ReasonReact.Router.push(amendementUrlPath);
                     self.send(CloseSnackbar);
                   }
                 )>
                 ("VOIR" |> ste)
               </MaterialUI.Button>
               <MaterialUI.IconButton
                 color=MaterialUI.IconButton.Color.Inherit
                 onClick=(_event => self.send(CloseSnackbar))>
                 <MaterialUIIcons.Close />
               </MaterialUI.IconButton>
             </span>
           autoHideDuration=6000
           message=
             <span>
               (
                 "Amendement "
                 ++ numeroProchainADiscuter
                 ++ {js| en discussion !|js}
                 |> ste
               )
             </span>
           onClose=((_event, _reason) => self.send(CloseSnackbar))
           _open=true
         />;
       | (_numeroProchainADiscuter, _openSnackbar) => ReasonReact.nullElement
       };
     } */
};
