open Model;

open Types;

let component = ReasonReact.statelessComponent("SenatSortEnSeance");

let make = (~sor: option(ameliSor), _children) => {
  ...component,
  render: _self =>
    <mark
      style=(
        ReactDOMRe.Style.make(
          ~backgroundColor=
            switch (sor) {
            | None => "grey"
            | Some(sor) =>
              Js.String.startsWith(sor.cod, "A") ? "#4caf50" : "#d0021b"
            },
          ~color="white",
          ~flex="1 1 auto",
          ~fontSize="16px",
          ~marginLeft="10px",
          ~padding="5px 5px 5px 5px",
          (),
        )
      )>
      (
        (
          switch (sor) {
          | None => {js|À discuter|js}
          | Some(sor) => sor.lib
          }
        )
        |> ste
      )
    </mark>,
};
