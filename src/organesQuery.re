open Configuration;

open Model;

let component = ReasonReact.statelessComponent("OrganesQuery");

let make = (~graphqlClient, ~legislature, _children) => {
  ...component,
  render: _self =>
    <GraphqlQuery
      graphqlClient query=(Apollo.createReferencesOrganesQuery(legislature))>
      ...(
           response =>
             switch (response) {
             | Loading =>
               <div>
                 <MaterialUI.AppBar position="static">
                   <MaterialUI.Toolbar>
                     <MaterialUI.Typography
                       color=MaterialUI.Typography.Color.Inherit
                       variant=MaterialUI.Typography.Variant.Title>
                       (config.appTitle |> ste)
                     </MaterialUI.Typography>
                   </MaterialUI.Toolbar>
                 </MaterialUI.AppBar>
                 <MaterialUI.LinearProgress />
               </div>
             | Failed(error) =>
               Js.log2("ReferencesOrganesQuery error", error);
               <div>
                 <MaterialUI.AppBar position="static">
                   <MaterialUI.Toolbar>
                     <MaterialUI.Typography
                       color=MaterialUI.Typography.Color.Inherit
                       variant=MaterialUI.Typography.Variant.Title>
                       (config.appTitle |> ste)
                     </MaterialUI.Typography>
                   </MaterialUI.Toolbar>
                 </MaterialUI.AppBar>
                 <div> ("Error" |> ste) </div>
               </div>;
             | Loaded(data) =>
               let referencesOrganes =
                 Decoders.referencesOrganesWrapper(data);
               <Organes referencesOrganes />;
             }
         )
    </GraphqlQuery>,
};
