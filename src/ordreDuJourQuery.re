open Configuration;

open Model;

let component = ReasonReact.statelessComponent("OrdreDuJourQuery");

let make = (~graphqlClient, ~legislature, ~organe, _children) => {
  ...component,
  render: _self =>
    <GraphqlQuery
      graphqlClient
      query=(Apollo.createOrdreDuJourQuery(legislature, organe))>
      ...(
           response =>
             switch (response) {
             | Loading =>
               <div>
                 <MaterialUI.AppBar position="static">
                   <MaterialUI.Toolbar>
                     <MaterialUI.Typography
                       color=MaterialUI.Typography.Color.Inherit
                       variant=MaterialUI.Typography.Variant.Title>
                       (config.appTitle |> ste)
                     </MaterialUI.Typography>
                   </MaterialUI.Toolbar>
                 </MaterialUI.AppBar>
                 <MaterialUI.LinearProgress />
               </div>
             | Failed(error) =>
               Js.log2("OrdreDuJourQuery error", error);
               <div>
                 <MaterialUI.AppBar position="static">
                   <MaterialUI.Toolbar>
                     <MaterialUI.Typography
                       color=MaterialUI.Typography.Color.Inherit
                       variant=MaterialUI.Typography.Variant.Title>
                       (config.appTitle |> ste)
                     </MaterialUI.Typography>
                   </MaterialUI.Toolbar>
                 </MaterialUI.AppBar>
                 <div> ("Error" |> ste) </div>
               </div>;
             | Loaded(data) =>
               let ordreDuJour = Decoders.ordreDuJourWrapper(data);
               <OrdreDuJour legislature ordreDuJour organe />;
             }
         )
    </GraphqlQuery>,
};
