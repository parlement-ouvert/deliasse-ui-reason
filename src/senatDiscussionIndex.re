open Model;

open Types;

module StringsSetItem = {
  type t = string;
  let compare = Pervasives.compare;
};

module StringsSet = Set.Make(StringsSetItem);

let component = ReasonReact.statelessComponent("SenatDiscussionIndex");

let make = (~onNavigateToAmendement, ~txt: ameliTxt, _children) => {
  ...component,
  render: _self =>
    <SenatDiscussionLayout onNavigateToAmendement txt>
      <MaterialUI.Typography
        color=MaterialUI.Typography.Color.Inherit
        component="h1"
        variant=MaterialUI.Typography.Variant.Display2>
        (
          txt.nat.lib
          ++ " "
          ++ Belt.Option.getWithDefault(txt.inl, txt.int)
          |> ste
        )
      </MaterialUI.Typography>
      (
        txt.subs
        |> Array.map((sub: ameliSub) =>
             <section
               id=(
                 Slugify.slugify(
                   Belt.Option.getWithDefault(
                     sub.lic,
                     string_of_int(sub.id),
                   ),
                 )
               )
               key=(string_of_int(sub.id))>
               <MaterialUI.Typography
                 align=MaterialUI.Typography.Align.Center
                 color=MaterialUI.Typography.Color.Inherit
                 component="h2"
                 variant=MaterialUI.Typography.Variant.Display1>
                 (
                   Belt.Option.getWithDefault(sub.lib, string_of_int(sub.id))
                   |> ste
                 )
               </MaterialUI.Typography>
               (
                 txt.amds
                 |> Array.to_list
                 |> List.filter((amd: ameliAmd) =>
                      Belt.Option.getWithDefault(amd.subid, -1) === sub.id
                    )
                 |> Array.of_list
                 |> Array.map((amd: ameliAmd) => {
                      let hash =
                        Slugify.slugify(
                          Belt.Option.getWithDefault(
                            amd.num,
                            string_of_int(amd.id),
                          ),
                        );
                      <article key=(string_of_int(amd.id))>
                        <a
                          href=("tout#" ++ hash)
                          onClick=(
                            event => {
                              ReactEventRe.Mouse.preventDefault(event);
                              ReactEventRe.Mouse.stopPropagation(event);
                              onNavigateToAmendement(
                                Belt.Option.getWithDefault(
                                  amd.num,
                                  string_of_int(amd.id),
                                ),
                              );
                            }
                          )>
                          <MaterialUI.Typography
                            color=MaterialUI.Typography.Color.Inherit
                            component="h4"
                            variant=MaterialUI.Typography.Variant.Title>
                            (
                              "Amendement "
                              ++ Belt.Option.getWithDefault(
                                   amd.num,
                                   string_of_int(amd.id),
                                 )
                              |> ste
                            )
                          </MaterialUI.Typography>
                        </a>
                        /* (
                             switch txt.numeroProchainADiscuter {
                             | None => ReasonReact.nullElement
                             | Some(numeroProchainADiscuter) =>
                               if (numeroProchainADiscuter
                                   == amendementWrapper.numero) {
                                 <span
                                   style=(
                                     ReactDOMRe.Style.make(~color="orange", ())
                                   )>
                                   <MaterialUIIcons.Whatshot />
                                 </span>;
                               } else {
                                 ReasonReact.nullElement;
                               }
                             }
                           ) */
                        <SenatSortEnSeance sor=amd.sor />
                      </article>;
                    })
                 |> ReasonReact.arrayToElement
               )
             </section>
           )
        |> ReasonReact.arrayToElement
      )
    </SenatDiscussionLayout>,
};
