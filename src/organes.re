open Model;

open Types;

let component = ReasonReact.statelessComponent("Organe");

let make = (~referencesOrganes: array(referenceOrgane), _children) => {
  ...component,
  render: _self =>
    <ul>
      (
        referencesOrganes
        |> Array.map(referenceOrgane =>
             <li key=referenceOrgane.value> (referenceOrgane.text |> ste) </li>
           )
        |> ReasonReact.arrayToElement
      )
    </ul>,
};
