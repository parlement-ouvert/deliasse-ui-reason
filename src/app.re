type page =
  | AboutPage
  | GraphqlPage(list(string), string);

type action =
  | Navigate(page);

type state = {
  page,
  todo: int,
};

let component = ReasonReact.reducerComponent("App");

let urlToPage = (url: ReasonReact.Router.url) : page =>
  switch (url.path) {
  | ["a-propos"] => AboutPage
  | _ => GraphqlPage(url.path, url.hash)
  };

let make = (~message as _message, _children) => {
  ...component,
  initialState: () => {
    page: urlToPage(ReasonReact.Router.dangerouslyGetInitialUrl()),
    todo: 0,
  },
  subscriptions: self => [
    Sub(
      () =>
        ReasonReact.Router.watchUrl(url =>
          self.send(Navigate(urlToPage(url)))
        ),
      ReasonReact.Router.unwatchUrl,
    ),
  ],
  reducer: (action, state) =>
    switch (action) {
    | Navigate(page) => ReasonReact.Update({...state, page})
    },
  render: self =>
    switch (self.state.page) {
    | AboutPage => <About />
    | GraphqlPage(path, hash) => <GraphqlClientQuery hash path />
    },
};
