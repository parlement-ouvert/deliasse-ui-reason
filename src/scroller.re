%bs.raw
{|var scrolls = require("./scrolls")|};

let scrollToId: string => unit = [%bs.raw
  {|
function scrollToId(id) {
  scrolls.scrollToId(id, {
	  align: "top",
	  duration: 1,
    offset: 0
  });
}
  |}
];

let scrollToRef: ReasonReact.reactRef => unit = [%bs.raw
  {|
function scrollToRef(ref) {
  scrolls.scrollToRef(ref, {
	  align: "top",
	  duration: 1,
    offset: 0
  });
}
  |}
];
