open Configuration;

open Model;

[%bs.raw {|require('./about.css')|}];

[@bs.module] external logoParlementLive : string = "./logo-parlement-live.jpg";

[@bs.module] external logoReact : string = "./logo-react.svg";

let component = ReasonReact.statelessComponent("About");

let make = _children => {
  ...component,
  render: _self =>
    <div>
      <MaterialUI.AppBar position="static">
        <MaterialUI.Toolbar>
          <MaterialUI.Typography
            color=MaterialUI.Typography.Color.Inherit
            variant=MaterialUI.Typography.Variant.Title>
            (config.appTitle |> ste)
          </MaterialUI.Typography>
        </MaterialUI.Toolbar>
      </MaterialUI.AppBar>
      <MaterialUI.Typography
        color=MaterialUI.Typography.Color.Inherit
        variant=MaterialUI.Typography.Variant.Title>
        ({|À propos de |} ++ config.appTitle |> ste)
        <img src=logoReact className="logo-react" alt="Logo de React" />
        <img
          src=logoParlementLive
          className="logo-react"
          alt="Logo du Parlement Live"
        />
        <div> <MaterialUIIcons.Delete /> </div>
      </MaterialUI.Typography>
    </div>,
};
