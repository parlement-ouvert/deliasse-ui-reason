open Configuration;

open Model;

let component = ReasonReact.statelessComponent("SenatDiscussionQuery");

let make =
    (~graphqlClient, ~hash, ~num, ~path, ~sesinsLil, ~txttyp, _children) => {
  ...component,
  render: _self =>
    <GraphqlQuery
      graphqlClient
      query=(Apollo.createSenatDiscussionQuery(num, sesinsLil, txttyp))>
      ...(
           response =>
             switch (response) {
             | Loading =>
               <div>
                 <MaterialUI.AppBar position="static">
                   <MaterialUI.Toolbar>
                     <MaterialUI.Typography
                       color=MaterialUI.Typography.Color.Inherit
                       variant=MaterialUI.Typography.Variant.Title>
                       (config.appTitle |> ste)
                     </MaterialUI.Typography>
                   </MaterialUI.Toolbar>
                 </MaterialUI.AppBar>
                 <MaterialUI.LinearProgress />
               </div>
             | Failed(error) =>
               Js.log2("SenatDiscussionQuery error", error);
               <div>
                 <MaterialUI.AppBar position="static">
                   <MaterialUI.Toolbar>
                     <MaterialUI.Typography
                       color=MaterialUI.Typography.Color.Inherit
                       variant=MaterialUI.Typography.Variant.Title>
                       (config.appTitle |> ste)
                     </MaterialUI.Typography>
                   </MaterialUI.Toolbar>
                 </MaterialUI.AppBar>
                 <div> ("Error" |> ste) </div>
               </div>;
             | Loaded(data) =>
               let txt = Decoders.senatDiscussionWrapper(data);
               <SenatDiscussion graphqlClient hash path txt />;
             }
         )
    </GraphqlQuery>,
};
